package com.jhedeen.pioneer.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jhedeen.pioneer.model.UserDto;
import com.jhedeen.pioneer.service.UserService;
import com.jhedeen.pioneer.service.exceptions.DuplicateEntityException;
import com.jhedeen.pioneer.service.exceptions.RecordNotFoundException;
import com.jhedeen.pioneer.service.exceptions.ValidationException;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static com.jhedeen.pioneer.testutil.UserHelper.ADMIN_LOGIN;
import static com.jhedeen.pioneer.testutil.UserHelper.ADMIN_PASSWORD;
import static com.jhedeen.pioneer.testutil.UserHelper.CLIENT_LOGIN;
import static com.jhedeen.pioneer.testutil.UserHelper.CLIENT_PASSWORD;
import static com.jhedeen.pioneer.testutil.UserHelper.PATH;
import static com.jhedeen.pioneer.testutil.UserHelper.createValidUser;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserControllerTest extends ControllerTest {

    private static final Integer ID = 10;
    private static final String EMAIL = "sa@gmail.com";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService service;

    @Test
    public void shouldReturnUserById() throws Exception {
        when(service.getById(ID)).thenReturn(createValidUser(ID));

        this.mockMvc.perform(get(PATH + ID).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(ID)))
                .andExpect(jsonPath("$.name", is("Степанов Александр")))
                .andExpect(jsonPath("$.role", is("CLIENT")));
    }

    @Test
    public void shouldReturnNotFoundById() throws Exception {
        when(service.getById(any(Integer.class))).thenThrow(RecordNotFoundException.class);

        this.mockMvc.perform(get(PATH + "404").with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD)))
                .andDo(print())
                .andExpect(status().is(404));
    }

    @Test
    public void shouldReturn403() throws Exception {
        this.mockMvc.perform(get(PATH + ID).with(httpBasic(CLIENT_LOGIN, CLIENT_PASSWORD)))
                .andDo(print())
                .andExpect(status().is(403));

        verify(service, never()).getById(ID);
    }

    @Test
    public void shouldReturn401() throws Exception {
        this.mockMvc.perform(get(PATH + ID).with(httpBasic(CLIENT_LOGIN, ADMIN_PASSWORD)))
                .andDo(print())
                .andExpect(status().is(401));
    }

    @Test
    public void shouldReturnUserByEmail() throws Exception {
        when(service.getByEmail(EMAIL)).thenReturn(createValidUser(ID));

        this.mockMvc.perform(get(PATH + "email?email=" + EMAIL).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(ID)))
                .andExpect(jsonPath("$.name", is("Степанов Александр")))
                .andExpect(jsonPath("$.role", is("CLIENT")));
    }

    @Test
    public void shouldReturnNotFoundByEmail() throws Exception {
        when(service.getByEmail(any(String.class))).thenThrow(RecordNotFoundException.class);

        this.mockMvc.perform(get(PATH + "email?email=not@found.com").with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD)))
                .andDo(print())
                .andExpect(status().is(404));
    }

    @Test
    public void shouldReturnNotAcceptableEmail() throws Exception {
        when(service.getByEmail(any(String.class))).thenThrow(new ValidationException("Email is not valid"));

        this.mockMvc.perform(get(PATH + "email?email=invalid.email").with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD)))
                .andDo(print())
                .andExpect(status().is(406))
                .andExpect(header().string("Error", "Email is not valid"));
    }

    @Test
    public void shouldDeleteUser() throws Exception {
        this.mockMvc.perform(delete(PATH + ID).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD)))
                .andDo(print())
                .andExpect(status().is(204));

        verify(service).delete(ID);
    }

    @Test
    public void shouldReturnAllUsers() throws Exception {
        when(service.getAllUsers()).thenReturn(Arrays.asList(createValidUser(2), createValidUser(3)));

        this.mockMvc.perform(get(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*].id", containsInAnyOrder(2, 3)));

        verify(service).getAllUsers();
    }

    @Test
    public void shouldSaveNewUser() throws Exception {
        UserDto neo = createValidUser(null);
        UserDto saved = createValidUser(ID);
        when(service.save(any(UserDto.class))).thenReturn(saved);

        this.mockMvc.perform(post(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(new ObjectMapper().writeValueAsString(neo))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(header().string("Location", "http://localhost" + PATH + ID))
                .andExpect(content().string(""))
                .andExpect(status().is(201));

        verify(service).save(neo);
    }

    @Test
    public void shouldNotSaveDuplicate() throws Exception {
        UserDto neo = createValidUser(null);
        when(service.save(any(UserDto.class))).thenThrow(DuplicateEntityException.class);

        this.mockMvc.perform(post(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(new ObjectMapper().writeValueAsString(neo))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(header().string("Error", "This email already registered"))
                .andExpect(status().is(406));
    }

    @Test
    public void shouldNotSaveUserWithId() throws Exception {
        UserDto neo = createValidUser(2);

        this.mockMvc.perform(post(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(new ObjectMapper().writeValueAsString(neo))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(405));
    }

    @Test
    public void shouldUpdateUser() throws Exception {
        UserDto neo = createValidUser(2);
        this.mockMvc.perform(put(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(new ObjectMapper().writeValueAsString(neo))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(content().string(""))
                .andExpect(status().is(204));

        verify(service).update(neo);
    }

    @Test
    public void shouldNotUpdateNewUser() throws Exception {
        UserDto neo = createValidUser(null);

        this.mockMvc.perform(put(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(new ObjectMapper().writeValueAsString(neo))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(405));
    }

    @Test
    public void shouldThrowInvalidEmail() throws Exception {
        UserDto neo = createValidUser(null);
        String invalidEmail = "errEmail";

        when(service.save(any(UserDto.class))).thenThrow(new ValidationException(invalidEmail));

        this.mockMvc.perform(post(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(new ObjectMapper().writeValueAsString(neo))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(400))
                .andExpect(header().string("Error", invalidEmail));

        verify(service).save(neo);
    }

    @Test
    public void shouldThrowInvalidName() throws Exception {
        UserDto neo = createValidUser(null);
        String invalidName = "errName";

        when(service.save(any(UserDto.class))).thenThrow(new ValidationException(invalidName));

        this.mockMvc.perform(post(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(new ObjectMapper().writeValueAsString(neo))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(400))
                .andExpect(header().string("Error", invalidName));

        verify(service).save(neo);
    }

    @Test
    public void shouldThrowInvalidData() throws Exception {
        UserDto neo = createValidUser(null);
        String invalidName = "errName:errEmail:errPassLen";

        when(service.save(any(UserDto.class))).thenThrow(new ValidationException(invalidName));

        this.mockMvc.perform(post(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(new ObjectMapper().writeValueAsString(neo))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(400))
                .andExpect(header().stringValues("Error", "errName", "errEmail", "errPassLen"));

        verify(service).save(neo);
    }
}