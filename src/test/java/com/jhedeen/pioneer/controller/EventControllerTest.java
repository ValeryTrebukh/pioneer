package com.jhedeen.pioneer.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.jhedeen.pioneer.model.EventDto;
import com.jhedeen.pioneer.service.EventService;
import com.jhedeen.pioneer.service.exceptions.DuplicateEntityException;
import com.jhedeen.pioneer.service.exceptions.ValidationException;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;

import static com.jhedeen.pioneer.testutil.EventHelper.createValidEvent;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class EventControllerTest extends ControllerTest {

    private static final String PATH = "/events/";
    private static final String ADMIN_LOGIN = "ro@gmail.com";
    private static final String ADMIN_PASSWORD = "admin";
    private static final String CLIENT_LOGIN = "te@gmail.com";
    private static final String CLIENT_PASSWORD = "password";
    private static final int ID = 3;
    private static final LocalDate FEB_13 = LocalDate.parse("2019-02-13");
    private static ObjectMapper mapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    EventService service;

    @BeforeClass
    public static void setup() {
        mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    @Test
    public void shouldReturnEventById() throws Exception {
        when(service.getEvent(ID)).thenReturn(createValidEvent(ID));

        this.mockMvc.perform(get(PATH + ID))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(3)))
                .andExpect(jsonPath("$.date", is("2019-02-13")));
    }

    @Test
    public void shouldReturnEventsForRequestedDate() throws Exception {
        when(service.getAllEvents(FEB_13)).thenReturn(Arrays.asList(createValidEvent(2), createValidEvent(3)));

        this.mockMvc.perform(get(PATH + "/date?date=2019-02-13"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*].id", containsInAnyOrder(2, 3)));

        verify(service).getAllEvents(FEB_13);
    }

    @Test
    public void shouldReturnTodayEventsForEmptyDate() throws Exception {
        when(service.getAllEvents(LocalDate.now())).thenReturn(Collections.emptyList());

        this.mockMvc.perform(get(PATH + "/date?date="))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("[]"));

        verify(service).getAllEvents(LocalDate.now());
    }

    @Test
    public void shouldReturnTodayEventsForInvalidDate() throws Exception {
        when(service.getAllEvents(LocalDate.now())).thenReturn(Collections.emptyList());

        this.mockMvc.perform(get(PATH + "/date?date=monday"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("[]"));

        verify(service).getAllEvents(LocalDate.now());
    }

    @Test
    public void shouldDeleteEvent() throws Exception {
        this.mockMvc.perform(delete(PATH + ID).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD)))
                .andDo(print())
                .andExpect(status().is(204));

        verify(service).delete(ID);
    }

    @Test
    public void shouldSaveEvent() throws Exception {
        EventDto event = createValidEvent(null);
        EventDto saved = createValidEvent(ID);
        when(service.save(any(EventDto.class))).thenReturn(saved);

        this.mockMvc.perform(post(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(mapper.writeValueAsString(event))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(header().string("Location", "http://localhost" + PATH + ID))
                .andExpect(content().string(""))
                .andExpect(status().is(201));

        verify(service).save(event);
    }

    @Test
    public void shouldNotSaveEventWithId() throws Exception {
        EventDto event = createValidEvent(ID);

        this.mockMvc.perform(post(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(mapper.writeValueAsString(event))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(422));
    }

    @Test
    public void shouldFailValidation() throws Exception {
        EventDto event = createValidEvent(null);
        when(service.save(any(EventDto.class))).thenThrow(ValidationException.class);

        this.mockMvc.perform(post(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(mapper.writeValueAsString(event))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(422));
    }

    @Test
    public void shouldNotSaveEventDuplicate() throws Exception {
        EventDto event = createValidEvent(null);
        when(service.save(any(EventDto.class))).thenThrow(DuplicateEntityException.class);

        this.mockMvc.perform(post(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(mapper.writeValueAsString(event))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(header().string("Error", "There is already event on this timeslot."))
                .andExpect(status().is(406));
    }

    @Test
    public void shouldReturn403() throws Exception {
        this.mockMvc.perform(delete(PATH + ID).with(httpBasic(CLIENT_LOGIN, CLIENT_PASSWORD)))
                .andDo(print())
                .andExpect(status().is(403));
    }

    @Test
    public void shouldReturn401() throws Exception {
        this.mockMvc.perform(delete(PATH + ID).with(httpBasic(CLIENT_LOGIN, ADMIN_PASSWORD)))
                .andDo(print())
                .andExpect(status().is(401));
    }
}