package com.jhedeen.pioneer.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.jhedeen.pioneer.model.EventDto;
import com.jhedeen.pioneer.model.TicketDto;
import com.jhedeen.pioneer.model.UserDto;
import com.jhedeen.pioneer.service.TicketService;
import com.jhedeen.pioneer.service.exceptions.DuplicateEntityException;
import com.jhedeen.pioneer.service.exceptions.ValidationException;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.List;

import static com.jhedeen.pioneer.testutil.TicketHelper.createValidTicket;
import static com.jhedeen.pioneer.testutil.UserHelper.ADMIN_LOGIN;
import static com.jhedeen.pioneer.testutil.UserHelper.ADMIN_PASSWORD;
import static com.jhedeen.pioneer.testutil.UserHelper.CLIENT_LOGIN;
import static com.jhedeen.pioneer.testutil.UserHelper.CLIENT_PASSWORD;
import static org.hamcrest.Matchers.containsString;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TicketControllerTest extends ControllerTest {

    private static final String PATH = "/tickets";
    private static ObjectMapper mapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    TicketService service;

    @BeforeClass
    public static void setup() {
        mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    @Test
    public void shouldReturnTicketsForEvent() throws Exception {
        when(service.getAllTicketsByEvent(any(EventDto.class))).thenReturn(Collections.singletonList(createValidTicket(2)));

        this.mockMvc.perform(get(PATH + "/event/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("\"time\":\"13:00:00\"")))
                .andExpect(content().string(containsString("\"date\":\"2019-02-13\"")))
                .andExpect(content().string(containsString("\"rowNumber\":2")))
                .andExpect(content().string(containsString("\"seatNumber\":2")))
                .andExpect(content().string(containsString("\"name\":\"Terminator\"")));
    }

    @Test
    public void shouldReturnTicketsForUser() throws Exception {
        when(service.getAllTicketsByUser(any(UserDto.class))).thenReturn(Collections.singletonList(createValidTicket(2)));

        this.mockMvc.perform(get(PATH + "/user/2").with(httpBasic(CLIENT_LOGIN, CLIENT_PASSWORD)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("\"time\":\"13:00:00\"")))
                .andExpect(content().string(containsString("\"date\":\"2019-02-13\"")))
                .andExpect(content().string(containsString("\"rowNumber\":2")))
                .andExpect(content().string(containsString("\"seatNumber\":2")))
                .andExpect(content().string(containsString("\"name\":\"Terminator\"")));
    }

    @Test
    public void shouldReturn401ForUnauthorized() throws Exception {
        when(service.getAllTicketsByUser(any(UserDto.class))).thenReturn(Collections.singletonList(createValidTicket(2)));

        this.mockMvc.perform(get(PATH + "/user/2"))
                .andDo(print())
                .andExpect(status().is(401));
    }

    @Test
    public void shouldReturnActualTicketsForUser() throws Exception {
        when(service.getActualTicketsByUser(any(UserDto.class))).thenReturn(Collections.singletonList(createValidTicket(2)));

        this.mockMvc.perform(get(PATH + "/user-actual/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("\"time\":\"13:00:00\"")))
                .andExpect(content().string(containsString("\"date\":\"2019-02-13\"")))
                .andExpect(content().string(containsString("\"rowNumber\":2")))
                .andExpect(content().string(containsString("\"seatNumber\":2")))
                .andExpect(content().string(containsString("\"name\":\"Terminator\"")));
    }

    @Test
    public void shouldSaveTickets() throws Exception {
        List<TicketDto> tickets = Collections.singletonList(createValidTicket(2));

        this.mockMvc.perform(post(PATH).with(httpBasic(CLIENT_LOGIN, CLIENT_PASSWORD))
                .content(mapper.writeValueAsString(tickets))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(content().string(""))
                .andExpect(status().is(200));

        verify(service).saveAll(tickets);
    }

    @Test
    public void shouldReturn401() throws Exception {
        List<TicketDto> tickets = Collections.singletonList(createValidTicket(2));

        this.mockMvc.perform(post(PATH)
                .content(mapper.writeValueAsString(tickets))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(content().string(""))
                .andExpect(status().is(401));
    }

    @Test
    public void shouldReturn422() throws Exception {
        TicketDto ticket = createValidTicket(null);
        doThrow(ValidationException.class).when(service).saveAll(any());

        this.mockMvc.perform(post(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(mapper.writeValueAsString(Collections.singletonList(ticket)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(422));
    }

    @Test
    public void shouldReturn406() throws Exception {
        TicketDto ticket = createValidTicket(null);
        doThrow(DuplicateEntityException.class).when(service).saveAll(any());

        this.mockMvc.perform(post(PATH).with(httpBasic(CLIENT_LOGIN, CLIENT_PASSWORD))
                .content(mapper.writeValueAsString(Collections.singletonList(ticket)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(406));
    }
}
