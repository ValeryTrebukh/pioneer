package com.jhedeen.pioneer.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jhedeen.pioneer.model.MovieDto;
import com.jhedeen.pioneer.service.MovieService;
import com.jhedeen.pioneer.service.exceptions.RecordNotFoundException;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static com.jhedeen.pioneer.testutil.MovieHelper.createValidMovie;
import static com.jhedeen.pioneer.testutil.UserHelper.ADMIN_LOGIN;
import static com.jhedeen.pioneer.testutil.UserHelper.ADMIN_PASSWORD;
import static com.jhedeen.pioneer.testutil.UserHelper.CLIENT_LOGIN;
import static com.jhedeen.pioneer.testutil.UserHelper.CLIENT_PASSWORD;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.is;

public class MovieControllerTest extends ControllerTest {

    private static final Integer ID = 1;
    private static final String PATH = "/movies/";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MovieService service;

    @Test
    public void shouldReturnMovieById() throws Exception {
        when(service.get(ID)).thenReturn(createValidMovie(2));

        this.mockMvc.perform(get(PATH + ID).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(2)))
                .andExpect(jsonPath("$.name", is("Terminator")))
                .andExpect(jsonPath("$.genre", is("science-fiction action")))
                .andExpect(jsonPath("$.duration", is(107)))
                .andExpect(jsonPath("$.year", is(1984)))
                .andExpect(jsonPath("$.active", is(true)));
    }

    @Test
    public void shouldReturnNotFoundById() throws Exception {
        when(service.get(any(Integer.class))).thenThrow(RecordNotFoundException.class);

        this.mockMvc.perform(get(PATH + "404").with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD)))
                .andDo(print())
                .andExpect(status().is(404));
    }

    @Test
    public void shouldReturnAllMovies() throws Exception {
        when(service.getAllMovies()).thenReturn(Arrays.asList(createValidMovie(2), createValidMovie(3)));

        this.mockMvc.perform(get(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD)))
                .andDo(print())
                .andExpect(status().isOk()).andExpect(jsonPath("$[*].id", containsInAnyOrder(2, 3)));

        verify(service).getAllMovies();
    }

    @Test
    public void shouldReturn403() throws Exception {
        this.mockMvc.perform(get(PATH).with(httpBasic(CLIENT_LOGIN, CLIENT_PASSWORD)))
                .andDo(print())
                .andExpect(status().is(403));

        verify(service, never()).getAllMovies();
    }

    @Test
    public void shouldReturn401() throws Exception {
        this.mockMvc.perform(get(PATH).with(httpBasic(ADMIN_LOGIN, CLIENT_PASSWORD)))
                .andDo(print())
                .andExpect(status().is(401));

        verify(service, never()).getAllMovies();
    }

    @Test
    public void shouldReturnActiveMovies() throws Exception {
        when(service.getActiveMovies()).thenReturn(Arrays.asList(createValidMovie(2), createValidMovie(3)));

        this.mockMvc.perform(get(PATH + "active").with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*].id", containsInAnyOrder(2, 3)));

        verify(service).getActiveMovies();
    }

    @Test
    public void shouldDeleteMovie() throws Exception {
        this.mockMvc.perform(delete(PATH + ID).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD)))
                .andDo(print())
                .andExpect(status().is(204))
                .andExpect(content().string(""));

        verify(service).delete(ID);
    }

    @Test
    public void shouldSaveNewMovie() throws Exception {
        MovieDto movie = createValidMovie(null);
        MovieDto saved = createValidMovie(2);
        when(service.save(any(MovieDto.class))).thenReturn(saved);

        this.mockMvc.perform(post(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(new ObjectMapper().writeValueAsString(movie))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(header().string("Location", "http://localhost/movies/2"))
                .andExpect(content().string(""))
                .andExpect(status().is(201));

        verify(service).save(movie);
    }

    @Test
    public void shouldNotSaveMovieWithId() throws Exception {
        MovieDto movie = createValidMovie(2);

        this.mockMvc.perform(post(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(new ObjectMapper().writeValueAsString(movie))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(405));
    }

    @Test
    public void shouldUpdateExistingMovie() throws Exception {
        MovieDto movie = createValidMovie(2);
        this.mockMvc.perform(put(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(new ObjectMapper().writeValueAsString(movie))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(content().string(""))
                .andExpect(status().is(204));

        verify(service).update(movie);
    }

    @Test
    public void shouldNotUpdateNewMovie() throws Exception {
        MovieDto movie = createValidMovie(null);
        this.mockMvc.perform(put(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(new ObjectMapper().writeValueAsString(movie))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(405));
    }

    @Test
    public void shouldNotUpdateInvalidMovie() throws Exception {
        MovieDto movie = createValidMovie(2);
        movie.setName(null);
        this.mockMvc.perform(put(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(new ObjectMapper().writeValueAsString(movie))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(422));
    }
}