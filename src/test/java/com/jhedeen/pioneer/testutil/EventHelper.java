package com.jhedeen.pioneer.testutil;

import com.jhedeen.pioneer.entity.EventEntity;
import com.jhedeen.pioneer.entity.SeanceEntity;
import com.jhedeen.pioneer.model.EventDto;
import com.jhedeen.pioneer.model.SeanceDto;

import java.time.LocalDate;
import java.time.LocalTime;

import static com.jhedeen.pioneer.testutil.MovieHelper.createValidMovie;
import static com.jhedeen.pioneer.testutil.MovieHelper.createValidMovieEntity;

public class EventHelper {

    public static final LocalDate FEB_13 = LocalDate.parse("2019-02-13");

    public static EventEntity createValidEventEntity(Integer id) {
        SeanceEntity seance = new SeanceEntity();
        seance.setId(2);
        seance.setTime(LocalTime.of(13, 0));

        EventEntity event = new EventEntity();
        event.setId(id);
        event.setMovie(createValidMovieEntity(1));
        event.setSeance(seance);
        event.setDate(FEB_13);

        return event;
    }

    public static EventDto createValidEvent(Integer id) {
        SeanceDto seance = new SeanceDto();
        seance.setId(2);
        seance.setTime(LocalTime.of(13, 0));

        EventDto event = new EventDto();
        event.setMovie(createValidMovie(1));
        event.setSeance(seance);
        event.setDate(FEB_13);
        event.setId(id);

        return event;
    }
}
