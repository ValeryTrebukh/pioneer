package com.jhedeen.pioneer.testutil;

import com.jhedeen.pioneer.entity.MovieEntity;
import com.jhedeen.pioneer.model.MovieDto;

public class MovieHelper {

    public static MovieEntity createValidMovieEntity(Integer id) {
        MovieEntity movie = new MovieEntity();
        movie.setId(id);
        movie.setName("Terminator");
        movie.setGenre("science-fiction action");
        movie.setDuration(107);
        movie.setYear(1984);
        movie.setActive(true);

        return movie;
    }

    public static MovieDto createValidMovie(Integer id) {
        MovieDto movie = new MovieDto();
        movie.setId(id);
        movie.setName("Terminator");
        movie.setGenre("science-fiction action");
        movie.setDuration(107);
        movie.setYear(1984);
        movie.setActive(true);

        return movie;
    }
}
