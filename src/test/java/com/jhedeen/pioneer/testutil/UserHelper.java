package com.jhedeen.pioneer.testutil;

import com.jhedeen.pioneer.entity.UserEntity;
import com.jhedeen.pioneer.model.UserDto;

public class UserHelper {

    public static final String PATH = "/users/";
    public static final String ADMIN_LOGIN = "ro@gmail.com";
    public static final String ADMIN_PASSWORD = "admin";
    public static final String CLIENT_LOGIN = "te@gmail.com";
    public static final String CLIENT_PASSWORD = "password";

    public static UserEntity createValidUserEntity(Integer id) {
        UserEntity user = new UserEntity();
        user.setId(id);
        user.setEmail("sa@gmail.com");
        user.setName("Степанов Александр");
        user.setPassword("1111");
        user.setRole("CLIENT");

        return user;
    }

    public static UserDto createValidUser(Integer id) {
        UserDto user = new UserDto();
        user.setId(id);
        user.setEmail("sa@gmail.com");
        user.setName("Степанов Александр");
        user.setPassword("1111");
        user.setRole("CLIENT");

        return user;
    }
}
