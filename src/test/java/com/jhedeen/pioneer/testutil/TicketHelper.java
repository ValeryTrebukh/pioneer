package com.jhedeen.pioneer.testutil;

import com.jhedeen.pioneer.entity.TicketEntity;
import com.jhedeen.pioneer.model.TicketDto;

import static com.jhedeen.pioneer.testutil.EventHelper.createValidEvent;
import static com.jhedeen.pioneer.testutil.EventHelper.createValidEventEntity;
import static com.jhedeen.pioneer.testutil.UserHelper.createValidUser;
import static com.jhedeen.pioneer.testutil.UserHelper.createValidUserEntity;

public class TicketHelper {

    public static TicketEntity createValidTicketEntity(Integer id) {
        TicketEntity ticket = new TicketEntity();
        ticket.setUser(createValidUserEntity(2));
        ticket.setEvent(createValidEventEntity(2));
        ticket.setRowNumber(2);
        ticket.setSeatNumber(2);
        ticket.setId(id);

        return ticket;
    }

    public static TicketDto createValidTicket(Integer id) {
        TicketDto ticket = new TicketDto();
        ticket.setUser(createValidUser(2));
        ticket.setEvent(createValidEvent(2));
        ticket.setRowNumber(2);
        ticket.setSeatNumber(2);
        ticket.setId(id);

        return ticket;
    }
}
