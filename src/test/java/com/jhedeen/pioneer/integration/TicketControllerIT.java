package com.jhedeen.pioneer.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.jhedeen.pioneer.model.TicketDto;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Collections;

import static com.jhedeen.pioneer.testutil.TicketHelper.createValidTicket;
import static com.jhedeen.pioneer.testutil.UserHelper.ADMIN_LOGIN;
import static com.jhedeen.pioneer.testutil.UserHelper.ADMIN_PASSWORD;
import static com.jhedeen.pioneer.testutil.UserHelper.CLIENT_LOGIN;
import static com.jhedeen.pioneer.testutil.UserHelper.CLIENT_PASSWORD;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.containsString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
@ActiveProfiles("dev")
public class TicketControllerIT {

    private static ObjectMapper mapper;
    private static final String PATH = "/tickets";

    @Autowired
    private MockMvc mockMvc;

    @BeforeClass
    public static void setup() {
        mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    @Test
    public void shouldReturnTicketsForEvent() throws Exception {
        this.mockMvc.perform(get(PATH + "/event/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*].id", containsInAnyOrder(1, 2, 3, 4, 5, 6)));
    }

    @Test
    public void shouldReturnTicketsForUser() throws Exception {
        this.mockMvc.perform(get(PATH + "/user/2").with(httpBasic(CLIENT_LOGIN, CLIENT_PASSWORD)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*].id", containsInAnyOrder(1, 2, 3, 13)));
    }

    @Test
    public void shouldReturnActualTicketsForUser() throws Exception {
        this.mockMvc.perform(get(PATH + "/user-actual/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("\"time\":\"09:00:00\"")))
                .andExpect(content().string(containsString("\"date\":\"2119-02-14\"")))
                .andExpect(content().string(containsString("\"rowNumber\":5")))
                .andExpect(content().string(containsString("\"seatNumber\":6")))
                .andExpect(content().string(containsString("\"name\":\"Madagascar\"")));
    }

    @Test
    public void shouldSaveTickets() throws Exception {
        TicketDto ticket = createValidTicket(null);
        ticket.getEvent().setDate(LocalDate.now().plusDays(2));
        this.mockMvc.perform(post(PATH).with(httpBasic(CLIENT_LOGIN, CLIENT_PASSWORD))
                .content(mapper.writeValueAsString(Collections.singletonList(ticket)))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(content().string(""))
                .andExpect(status().is(200));
    }

    @Test
    public void shouldReturn422() throws Exception {
        this.mockMvc.perform(post(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(mapper.writeValueAsString(Collections.singletonList(createValidTicket(null))))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(422));
    }

    @Test
    public void shouldReturn406() throws Exception {
        TicketDto ticket = createValidTicket(null);
        ticket.setRowNumber(4);
        ticket.setSeatNumber(4);
        ticket.getEvent().setDate(LocalDate.now().plusDays(2));
        this.mockMvc.perform(post(PATH).with(httpBasic(CLIENT_LOGIN, CLIENT_PASSWORD))
                .content(mapper.writeValueAsString(Collections.singletonList(ticket)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(406));
    }
}
