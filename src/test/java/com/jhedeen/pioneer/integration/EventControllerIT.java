package com.jhedeen.pioneer.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.jhedeen.pioneer.model.EventDto;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

import static com.jhedeen.pioneer.testutil.EventHelper.createValidEvent;
import static com.jhedeen.pioneer.testutil.UserHelper.ADMIN_LOGIN;
import static com.jhedeen.pioneer.testutil.UserHelper.ADMIN_PASSWORD;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
@ActiveProfiles("dev")
public class EventControllerIT {
    private static final int ID = 2;
    private static final String PATH = "/events/";
    private static ObjectMapper mapper;

    @Autowired
    private MockMvc mockMvc;

    @BeforeClass
    public static void setup() {
        mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    @Test
    public void shouldReturnEventById() throws Exception {
        this.mockMvc.perform(get(PATH + ID))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(ID)))
                .andExpect(jsonPath("$.date", is("2019-02-13")));
    }

    @Test
    public void shouldReturnEventsForRequestedDate() throws Exception {
        this.mockMvc.perform(get(PATH + "/date?date=2019-02-13"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*].id", containsInAnyOrder(2, 1, 7)));
    }

    @Test
    public void shouldDeleteEvent() throws Exception {
        this.mockMvc.perform(delete(PATH + ID).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD)))
                .andDo(print())
                .andExpect(status().is(204));
    }

    @Test
    public void shouldSaveEvent() throws Exception {
        EventDto event = createValidEvent(null);
        event.setDate(LocalDate.parse("2019-05-13"));
        this.mockMvc.perform(post(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(mapper.writeValueAsString(event))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(header().string("Location", "http://localhost" + PATH + 12))
                .andExpect(content().string(""))
                .andExpect(status().is(201));
    }

    @Test
    public void shouldNotSaveEventWithId() throws Exception {
        this.mockMvc.perform(post(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(mapper.writeValueAsString(createValidEvent(ID)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(422));
    }

    @Test
    public void shouldFailValidation() throws Exception {
        EventDto event = createValidEvent(null);
        event.setMovie(null);
        event.setDate(LocalDate.parse("2019-05-13"));
        this.mockMvc.perform(post(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(mapper.writeValueAsString(event))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(422));
    }

    @Test
    public void shouldNotSaveEventDuplicate() throws Exception {
        this.mockMvc.perform(post(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(mapper.writeValueAsString(createValidEvent(null)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(header().string("Error", "There is already event on this timeslot."))
                .andExpect(status().is(406));
    }
}
