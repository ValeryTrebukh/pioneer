package com.jhedeen.pioneer.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jhedeen.pioneer.model.MovieDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static com.jhedeen.pioneer.testutil.MovieHelper.createValidMovie;
import static com.jhedeen.pioneer.testutil.UserHelper.ADMIN_LOGIN;
import static com.jhedeen.pioneer.testutil.UserHelper.ADMIN_PASSWORD;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
@ActiveProfiles("dev")
public class MovieControllerIT {
    private static final Integer ID = 1;
    private static final String PATH = "/movies/";

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldReturnMovieById() throws Exception {
        this.mockMvc.perform(get(PATH + ID).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("Terminator")))
                .andExpect(jsonPath("$.genre", is("science-fiction action")))
                .andExpect(jsonPath("$.duration", is(107)))
                .andExpect(jsonPath("$.year", is(1984)))
                .andExpect(jsonPath("$.active", is(true)));
    }

    @Test
    public void shouldReturnNotFoundById() throws Exception {
        this.mockMvc.perform(get(PATH + "404").with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD)))
                .andDo(print())
                .andExpect(status().is(404));
    }

    @Test
    public void shouldReturnAllMovies() throws Exception {
        this.mockMvc.perform(get(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD)))
                .andDo(print())
                .andExpect(status().isOk()).andExpect(jsonPath("$[*].id", containsInAnyOrder(1, 2, 3, 4, 5, 6, 7)));
    }

    @Test
    public void shouldReturnActiveMovies() throws Exception {
        this.mockMvc.perform(get(PATH + "active").with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*].id", containsInAnyOrder(1, 2, 3, 4, 5, 7)));
    }

    @Test
    public void shouldDeleteMovie() throws Exception {
        this.mockMvc.perform(delete(PATH + ID).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD)))
                .andDo(print())
                .andExpect(status().is(204))
                .andExpect(content().string(""));
    }

    @Test
    public void shouldSaveNewMovie() throws Exception {
        MovieDto movie = createValidMovie(null);

        this.mockMvc.perform(post(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(new ObjectMapper().writeValueAsString(movie))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(header().string("Location", "http://localhost/movies/8"))
                .andExpect(content().string(""))
                .andExpect(status().is(201));
    }

    @Test
    public void shouldNotSaveMovieWithId() throws Exception {
        MovieDto movie = createValidMovie(2);

        this.mockMvc.perform(post(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(new ObjectMapper().writeValueAsString(movie))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(405));
    }

    @Test
    public void shouldUpdateExistingMovie() throws Exception {
        this.mockMvc.perform(put(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(new ObjectMapper().writeValueAsString(createValidMovie(2)))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(content().string(""))
                .andExpect(status().is(204));
    }

    @Test
    public void shouldNotUpdateNewMovie() throws Exception {
        this.mockMvc.perform(put(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(new ObjectMapper().writeValueAsString(createValidMovie(null)))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(405));
    }

    @Test
    public void shouldNotUpdateInvalidMovie() throws Exception {
        MovieDto movie = createValidMovie(2);
        movie.setName(null);
        this.mockMvc.perform(put(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(new ObjectMapper().writeValueAsString(movie))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(422));
    }
}
