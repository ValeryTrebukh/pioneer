package com.jhedeen.pioneer.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jhedeen.pioneer.model.UserDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static com.jhedeen.pioneer.testutil.UserHelper.ADMIN_LOGIN;
import static com.jhedeen.pioneer.testutil.UserHelper.ADMIN_PASSWORD;
import static com.jhedeen.pioneer.testutil.UserHelper.PATH;
import static com.jhedeen.pioneer.testutil.UserHelper.createValidUser;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
@ActiveProfiles("dev")
public class UserControllerIT {

    private static final Integer ID = 1;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldFindUserById() throws Exception {
        this.mockMvc.perform(get(PATH + ID).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(ID)))
                .andExpect(jsonPath("$.name", is("Ромашова Ольга")))
                .andExpect(jsonPath("$.role", is("ADMIN")));
    }

    @Test
    public void shouldReturnNotFoundById() throws Exception {
        this.mockMvc.perform(get(PATH + "404").with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD)))
                .andDo(print())
                .andExpect(status().is(404));
    }

    @Test
    public void shouldFindUserByEmail() throws Exception {
        this.mockMvc.perform(get(PATH + "email?email=ro@gmail.com").with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(ID)))
                .andExpect(jsonPath("$.name", is("Ромашова Ольга")))
                .andExpect(jsonPath("$.role", is("ADMIN")));
    }

    @Test
    public void shouldReturnNotFoundByEmail() throws Exception {
        this.mockMvc.perform(get(PATH + "email?email=not@found.com").with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD)))
                .andDo(print())
                .andExpect(status().is(404));
    }

    @Test
    public void shouldReturnNotAcceptableEmail() throws Exception {
        this.mockMvc.perform(get(PATH + "email?email=invalid.email").with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD)))
                .andDo(print())
                .andExpect(status().is(406))
                .andExpect(header().string("Error", "Email is not valid"));
    }

    @Test
    public void shouldDeleteUser() throws Exception {
        this.mockMvc.perform(delete(PATH + ID).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD)))
                .andDo(print())
                .andExpect(status().is(204));
    }

    @Test
    public void shouldReturnAllUsers() throws Exception {
        this.mockMvc.perform(get(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*].id", containsInAnyOrder(1, 2, 3, 4, 5, 6, 7, 8, 9)));
    }

    @Test
    public void shouldSaveNewUser() throws Exception {
        UserDto neo = createValidUser(null);

        this.mockMvc.perform(post(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(new ObjectMapper().writeValueAsString(neo))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(201))
                .andExpect(header().string("Location", "http://localhost/users/10"))
                .andExpect(content().string(""));
    }

    @Test
    public void shouldNotSaveDuplicate() throws Exception {
        UserDto neo = createValidUser(null);
        neo.setEmail("te@gmail.com");

        this.mockMvc.perform(post(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(new ObjectMapper().writeValueAsString(neo))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(header().string("Error", "This email already registered"))
                .andExpect(status().is(406));
    }

    @Test
    public void shouldUpdateUser() throws Exception {
        UserDto neo = createValidUser(2);
        this.mockMvc.perform(put(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(new ObjectMapper().writeValueAsString(neo))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(content().string(""))
                .andExpect(status().is(204));
    }

    @Test
    public void shouldThrowInvalidEmailWhenSave() throws Exception {
        UserDto neo = createValidUser(null);
        neo.setEmail("terra.best");

        this.mockMvc.perform(post(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(new ObjectMapper().writeValueAsString(neo))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(400))
                .andExpect(header().string("Error", "errEmail"));
    }

    @Test
    public void shouldThrowInvalidNameWhenSave() throws Exception {
        UserDto neo = createValidUser(null);
        neo.setName("Peter");

        this.mockMvc.perform(post(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(new ObjectMapper().writeValueAsString(neo))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(400))
                .andExpect(header().string("Error", "errName"));
    }

    @Test
    public void shouldThrowInvalidDataWhenSave() throws Exception {
        UserDto user = new UserDto();
        user.setEmail("terra.best");
        user.setName("Peter");
        user.setPassword("123");

        this.mockMvc.perform(post(PATH).with(httpBasic(ADMIN_LOGIN, ADMIN_PASSWORD))
                .content(new ObjectMapper().writeValueAsString(user))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(400))
                .andExpect(header().stringValues("Error", "errName", "errEmail", "errPassLen"));
    }
}
