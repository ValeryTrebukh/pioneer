package com.jhedeen.pioneer.service.util;

import com.jhedeen.pioneer.model.UserDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(value = Parameterized.class)
public class UserDataValidatorTest {

    private UserDto userDto;
    private List<String> errors;

    public UserDataValidatorTest(UserDto userDto, List<String> errors) {
        this.userDto = userDto;
        this.errors = errors;
    }

    @Parameterized.Parameters(name= "{index}: checkForErrors({0})={1}")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {createTestUser("John Terry", "jt@mail.com", "test"), Collections.EMPTY_LIST},
                {createTestUser("JohnTerry", "jt@mail.com", "test"), Arrays.asList("errName")},
                {createTestUser("John Terry", "jmail.com", "test"), Arrays.asList("errEmail")},
                {createTestUser("John Terry", "jt@mail.com", "tes"), Arrays.asList("errPassLen")},
                {createTestUser("JohnTerry", "jtmail.com", "test"), Arrays.asList("errName", "errEmail")},
                {createTestUser("JohnTerry", "jt@mail.com", "tes"), Arrays.asList("errName", "errPassLen")},
                {createTestUser("John Terry", "jtmail.com", "tes"), Arrays.asList("errEmail", "errPassLen")},
                {createTestUser("JohnTerry", "jtmail.com", "tet"), Arrays.asList("errName", "errEmail", "errPassLen")},
        });
    }

    @Test
    public void shouldValidateUser() {
        List<String> actual = UserDataValidator.checkForErrors(userDto, true);
        assertThat(actual, is(errors));
    }

    private static UserDto createTestUser(String name, String email, String password) {
        UserDto user = new UserDto();
        user.setName(name);
        user.setEmail(email);
        user.setPassword(password);

        return user;
    }
}