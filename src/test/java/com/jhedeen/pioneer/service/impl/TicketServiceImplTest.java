package com.jhedeen.pioneer.service.impl;

import com.jhedeen.pioneer.entity.EventEntity;
import com.jhedeen.pioneer.entity.UserEntity;
import com.jhedeen.pioneer.model.TicketDto;
import com.jhedeen.pioneer.repository.TicketRepository;
import com.jhedeen.pioneer.service.ServiceTest;
import com.jhedeen.pioneer.service.TicketService;
import com.jhedeen.pioneer.service.exceptions.DuplicateEntityException;
import com.jhedeen.pioneer.service.exceptions.ValidationException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.jhedeen.pioneer.testutil.EventHelper.createValidEvent;
import static com.jhedeen.pioneer.testutil.TicketHelper.createValidTicket;
import static com.jhedeen.pioneer.testutil.UserHelper.createValidUser;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.hamcrest.Matchers.is;

public class TicketServiceImplTest extends ServiceTest {

    @Mock
    private TicketRepository repository;

    @InjectMocks
    private TicketService sut = new TicketServiceImpl();

    @Before
    public void setUp() {
        ReflectionTestUtils.setField(sut, "rowsCount", 9);
        ReflectionTestUtils.setField(sut, "seatsCount", 9);
    }

    @Test
    public void shouldReturnAllTicketsByEvent() {
        sut.getAllTicketsByEvent(createValidEvent(4));

        verify(repository).findAllByEvent(any(EventEntity.class));
    }

    @Test
    public void shouldReturnAllTicketsByUser() {
        sut.getAllTicketsByUser(createValidUser(4));

        verify(repository).findAllByUser(any(UserEntity.class));
    }

    @Test
    public void shouldReturnActualTicketsByUser() {
        sut.getActualTicketsByUser(createValidUser(4));

        verify(repository).findAllByUser(any(UserEntity.class));
    }

    @Test
    public void shouldSaveNewTickets() {
        TicketDto ticket = createValidTicket(null);
        ticket.getEvent().setDate(LocalDate.now().plusDays(2));
        sut.saveAll(Collections.singletonList(ticket));

        verify(repository).saveAll(any());
    }

    @Test(expected = DuplicateEntityException.class)
    public void shouldThrowExceptionForDuplicate() {
        List<TicketDto> tickets = new ArrayList<>();
        when(repository.saveAll(any())).thenThrow(DataIntegrityViolationException.class);

        sut.saveAll(tickets);
    }

    @Test
    public void shouldValidateForNullUser() {
        TicketDto ticket = createValidTicket(null);
        ticket.setUser(null);
        try{
            sut.saveAll(Collections.singletonList(ticket));
        } catch (ValidationException ve) {
            assertThat(ve.getMessage(), is("Invalid ticket data"));
        }
    }

    @Test
    public void shouldValidateForNullEvent() {
        TicketDto ticket = createValidTicket(null);
        ticket.setEvent(null);
        try{
            sut.saveAll(Collections.singletonList(ticket));
        } catch (ValidationException ve) {
            assertThat(ve.getMessage(), is("Invalid ticket data"));
        }
    }

    @Test
    public void shouldValidateForPastData() {
        TicketDto ticket = createValidTicket(null);
        try{
            sut.saveAll(Collections.singletonList(ticket));
        } catch (ValidationException ve) {
            assertThat(ve.getMessage(), is("Invalid ticket data"));
        }
    }

    @Test
    public void shouldValidateForInvalidRow() {
        TicketDto ticket = createValidTicket(null);
        ticket.setRowNumber(12);
        try{
            sut.saveAll(Collections.singletonList(ticket));
        } catch (ValidationException ve) {
            assertThat(ve.getMessage(), is("Invalid row number. Value out of range [1-9]"));
        }
    }

    @Test
    public void shouldValidateForInvalidSeat() {
        TicketDto ticket = createValidTicket(null);
        ticket.setSeatNumber(12);
        try{
            sut.saveAll(Collections.singletonList(ticket));
        } catch (ValidationException ve) {
            assertThat(ve.getMessage(), is("Invalid seat number. Value out of range [1-9]"));
        }
    }
}