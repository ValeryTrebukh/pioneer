package com.jhedeen.pioneer.service.impl;

import com.jhedeen.pioneer.entity.MovieEntity;
import com.jhedeen.pioneer.model.MovieDto;
import com.jhedeen.pioneer.repository.MovieRepository;
import com.jhedeen.pioneer.service.MovieService;
import com.jhedeen.pioneer.service.ServiceTest;
import com.jhedeen.pioneer.service.converter.MovieConverter;
import com.jhedeen.pioneer.service.exceptions.RecordNotFoundException;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.List;

import static com.jhedeen.pioneer.testutil.MovieHelper.createValidMovie;
import static com.jhedeen.pioneer.testutil.MovieHelper.createValidMovieEntity;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MovieServiceImplTest  extends ServiceTest {

    private static final int ID = 3;

    @Mock
    private MovieRepository repository;

    @InjectMocks
    private MovieService sut = new MovieServiceImpl();

    @Test
    public void shouldSaveMovie() {
        MovieDto movie = createValidMovie(null);
        when(repository.save(any(MovieEntity.class))).thenReturn(createValidMovieEntity(12));

        sut.save(movie);

        verify(repository).save(MovieConverter.toEntity(movie));
    }

    @Test
    public void shouldDeleteMovie() {
        sut.delete(ID);

        verify(repository).deleteById(ID);
    }

    @Test
    public void shouldFindById() {
        when(repository.findById(ID)).thenReturn(new MovieEntity());

        sut.get(ID);

        verify(repository).findById(ID);
    }

    @Test(expected = RecordNotFoundException.class)
    public void shouldThrowExceptionForFindById() {
        when(repository.findById(any(Integer.class))).thenReturn(null);

        sut.get(ID);
    }

    @Test
    public void shouldUpdate() {
        MovieDto movie = createValidMovie(null);

        sut.update(movie);

        verify(repository).save(MovieConverter.toEntity(movie));
    }

    @Test
    public void shouldReturnAllMovies() {
        sut.getAllMovies();

        verify(repository).findAll();
    }

    @Test
    public void shouldReturnActiveMovies() {
        MovieEntity movie_1 = createValidMovieEntity(12);
        MovieEntity movie_2 = createValidMovieEntity(13);
        movie_2.setActive(false);

        List<MovieEntity> movies = Arrays.asList(movie_1, movie_2);
        when(repository.findAll()).thenReturn(movies);

        List<MovieDto> activeMovies = sut.getActiveMovies();

        assertThat(activeMovies, hasSize(1));
        assertThat(activeMovies, is(Arrays.asList(createValidMovie(12))));
    }
}