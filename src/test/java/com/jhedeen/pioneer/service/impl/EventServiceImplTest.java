package com.jhedeen.pioneer.service.impl;

import com.jhedeen.pioneer.entity.EventEntity;
import com.jhedeen.pioneer.model.EventDto;
import com.jhedeen.pioneer.repository.EventRepository;
import com.jhedeen.pioneer.service.EventService;
import com.jhedeen.pioneer.service.ServiceTest;
import com.jhedeen.pioneer.service.converter.EventConverter;
import com.jhedeen.pioneer.service.exceptions.DuplicateEntityException;
import com.jhedeen.pioneer.service.exceptions.RecordNotFoundException;
import com.jhedeen.pioneer.service.exceptions.ValidationException;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.dao.DataIntegrityViolationException;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static com.jhedeen.pioneer.testutil.EventHelper.FEB_13;
import static com.jhedeen.pioneer.testutil.EventHelper.createValidEventEntity;
import static com.jhedeen.pioneer.testutil.EventHelper.createValidEvent;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class EventServiceImplTest  extends ServiceTest {

    private static final int ID = 3;

    @Mock
    private EventRepository repository;

    @InjectMocks
    private EventService sut = new EventServiceImpl();

    @Test
    public void shouldGetAllEventsDyDate() {
        List<EventEntity> entities = Arrays.asList(createValidEventEntity(4), createValidEventEntity(6));
        when(repository.findByDate(any(LocalDate.class))).thenReturn(entities);

        List<EventDto> events = sut.getAllEvents(FEB_13);

        verify(repository).findByDate(FEB_13);
        assertThat(events, hasSize(2));
    }

    @Test
    public void shouldDeleteEvent() {
        sut.delete(ID);

        verify(repository).deleteById(ID);
    }

    @Test
    public void shouldFindEventById() {
        when(repository.findById(ID)).thenReturn(createValidEventEntity(4));

        sut.getEvent(ID);

        verify(repository).findById(ID);
    }

    @Test(expected = RecordNotFoundException.class)
    public void shouldThrowNotFoundWhenFind() {
        when(repository.findById(anyInt())).thenReturn(null);

        sut.getEvent(88);
    }

    @Test
    public void shouldSaveEvent() {
        EventDto event = createValidEvent(null);
        when(repository.save(any(EventEntity.class))).thenReturn(createValidEventEntity(2));

        sut.save(event);

        verify(repository).save(EventConverter.toEntity(event));
    }

    @Test(expected = DuplicateEntityException.class)
    public void shouldThrowDuplicateExceptionWhenSaveEvent() {
        when(repository.save(any(EventEntity.class))).thenThrow(DataIntegrityViolationException.class);

        sut.save(createValidEvent(null));
    }

    @Test(expected = ValidationException.class)
    public void shouldThrowValidationExceptionForNullSeance() {
        EventDto event = createValidEvent(null);
        event.setMovie(null);

        sut.save(event);
    }

    @Test(expected = ValidationException.class)
    public void shouldThrowValidationExceptionForNullMovie() {
        EventDto event = createValidEvent(null);
        event.setSeance(null);

        sut.save(event);
    }

    @Test(expected = ValidationException.class)
    public void shouldThrowValidationExceptionForInactiveMovie() {
        EventDto event = createValidEvent(null);
        event.getMovie().setActive(false);

        sut.save(event);
    }
}