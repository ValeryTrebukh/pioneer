package com.jhedeen.pioneer.service.impl;

import com.jhedeen.pioneer.entity.UserEntity;
import com.jhedeen.pioneer.model.UserDto;
import com.jhedeen.pioneer.repository.UserRepository;
import com.jhedeen.pioneer.service.ServiceTest;
import com.jhedeen.pioneer.service.UserService;
import com.jhedeen.pioneer.service.exceptions.DuplicateEntityException;
import com.jhedeen.pioneer.service.exceptions.RecordNotFoundException;
import com.jhedeen.pioneer.service.exceptions.ValidationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.dao.DataIntegrityViolationException;

import static com.jhedeen.pioneer.testutil.UserHelper.createValidUserEntity;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UserServiceImplTest extends ServiceTest {

    private static final int ID = 1;

    @Mock
    private UserRepository repository;
    @Mock
    private UserDto userDto;

    @InjectMocks
    private UserService sut = new UserServiceImpl();

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Before
    public void setup() {
        when(userDto.getId()).thenReturn(ID);
        when(userDto.getPassword()).thenReturn("password");
        when(userDto.getName()).thenReturn("John Terry");
        when(userDto.getEmail()).thenReturn("jt@email.com");
    }

    @Test
    public void shouldSaveUser() {
        when(repository.save(any(UserEntity.class))).thenReturn(createValidUserEntity(12));

        UserDto saved = sut.save(userDto);

        verify(repository).save(any(UserEntity.class));
        assertThat(saved.getId(), is(12));
    }

    @Test
    public void shouldThrowValidationExceptionForEmail() {
        when(userDto.getEmail()).thenReturn("email.com");

        expectedEx.expect(ValidationException.class);
        expectedEx.expectMessage("errEmail");

        sut.save(userDto);
    }

    @Test
    public void shouldThrowValidationException() {
        when(userDto.getPassword()).thenReturn("");
        when(userDto.getName()).thenReturn("JohnTerry");
        when(userDto.getEmail()).thenReturn("email.com");

        expectedEx.expect(ValidationException.class);
        expectedEx.expectMessage("errName:errEmail:errPassLen");

        sut.save(userDto);
    }

    @Test(expected = DuplicateEntityException.class)
    public void shouldSaveThrowDuplicateException() {
        when(repository.save(any(UserEntity.class))).thenThrow(DataIntegrityViolationException.class);

        sut.save(userDto);
    }

    @Test
    public void shouldUpdateWithPassword() {
        sut.update(userDto);

        verify(repository).save(any(UserEntity.class));
    }

    @Test
    public void shouldUpdateWithoutPassword() {
        when(userDto.getPassword()).thenReturn("");
        when(userDto.getRole()).thenReturn("role");

        sut.update(userDto);

        verify(userDto, never()).setPassword(anyString());
        verify(repository).updateWithoutPassword(anyString(), anyString(), anyString(), anyInt());
    }

    @Test
    public void shouldUpdateThrowValidationExceptionForNameAndPassword() {
        when(userDto.getPassword()).thenReturn("111");
        when(userDto.getName()).thenReturn("JohnTerry");

        expectedEx.expect(ValidationException.class);
        expectedEx.expectMessage("errName:errPassLen");

        sut.update(userDto);
    }

    @Test
    public void shouldUpdateThrowValidationExceptionForName() {
        when(userDto.getPassword()).thenReturn("");
        when(userDto.getName()).thenReturn("JohnTerry");

        expectedEx.expect(ValidationException.class);
        expectedEx.expectMessage("errName");

        sut.update(userDto);
    }

    @Test(expected = DuplicateEntityException.class)
    public void shouldUpdateThrowDuplicateException() {
        when(repository.save(any(UserEntity.class))).thenThrow(DataIntegrityViolationException.class);

        sut.update(userDto);
    }

    @Test
    public void shouldDeleteUser() {
        sut.delete(ID);

        verify(repository).deleteById(ID);
    }

    @Test
    public void shouldFindUserById() {
        when(repository.findById(ID)).thenReturn(new UserEntity());

        sut.getById(ID);

        verify(repository).findById(ID);
    }

    @Test(expected = RecordNotFoundException.class)
    public void getByIdNotFound() {
        when(repository.findById(anyInt())).thenReturn(null);

        sut.getById(88);
    }

    @Test
    public void getByEmail() {
        String email = "some@email.com";
        when(repository.findByEmail(email)).thenReturn(new UserEntity());

        sut.getByEmail(email);

        verify(repository).findByEmail(email);
    }

    @Test(expected = RecordNotFoundException.class)
    public void getByEmailNotFound() {
        when(repository.findByEmail(anyString())).thenReturn(null);

        sut.getByEmail("dd@mail.com");
    }

    @Test(expected = ValidationException.class)
    public void getByEmailNotValid() {
        when(repository.findByEmail(anyString())).thenReturn(null);

        sut.getByEmail("dd.com");
    }

    @Test
    public void getAllUsers() {
        sut.getAllUsers();

        verify(repository).findAll();
    }
}