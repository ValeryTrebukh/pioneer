package com.jhedeen.pioneer.repository;

import com.jhedeen.pioneer.entity.MovieEntity;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static com.jhedeen.pioneer.testutil.MovieHelper.createValidMovieEntity;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class MovieRepositoryTest extends RepositoryTest {

    private static final int ID = 1;

    @Autowired
    MovieRepository sut;

    @Test
    public void shouldReturnMovie() {
        assertThat(createValidMovieEntity(ID), is(sut.findById(ID)));
    }

    @Test
    public void shouldReturnNullForNonExistingId() {
        assertNull(sut.findById(458));
    }

    @Test
    public void shouldReturnMovieList() {
        List<MovieEntity> movies = sut.findAll();

        assertThat(movies.size(), is(7));
    }

    @Test
    public void shouldSaveMovie() {
        MovieEntity newMovie = createValidMovieEntity(null);

        MovieEntity saved = sut.save(newMovie);

        assertThat(sut.findById(saved.getId()).get(), is(newMovie));
    }

    @Test
    public void shouldDeleteMovie() {
        assertNotNull(sut.findById(ID));

        sut.deleteById(ID);

        assertNull(sut.findById(ID));
    }
}