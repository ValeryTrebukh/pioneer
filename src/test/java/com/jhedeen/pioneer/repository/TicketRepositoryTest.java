package com.jhedeen.pioneer.repository;

import com.jhedeen.pioneer.entity.EventEntity;
import com.jhedeen.pioneer.entity.TicketEntity;
import com.jhedeen.pioneer.entity.UserEntity;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.Collections;
import java.util.List;

import static com.jhedeen.pioneer.testutil.EventHelper.createValidEventEntity;
import static com.jhedeen.pioneer.testutil.TicketHelper.createValidTicketEntity;
import static com.jhedeen.pioneer.testutil.UserHelper.createValidUserEntity;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

public class TicketRepositoryTest extends RepositoryTest {

    @Autowired
    TicketRepository sut;

    @Test
    public void findAllByUser() {
        UserEntity user = createValidUserEntity(2);

        assertThat(sut.findAllByUser(user), hasSize(4));
    }

    @Test
    public void findAllByEvent() {
        EventEntity event = createValidEventEntity(2);

        assertThat(sut.findAllByEvent(event), hasSize(6));
    }

    @Test
    public void shouldSaveTickets() {
        List<TicketEntity> tickets = Collections.singletonList(createTicket(2, 2));

        sut.saveAll(tickets);
     }

    @Test(expected = DataIntegrityViolationException.class)
    public void shouldFailSaveTickets() {
        List<TicketEntity> tickets = Collections.singletonList(createTicket(4, 4));

        sut.saveAll(tickets);
    }

    private TicketEntity createTicket(int row, int seat) {
        TicketEntity ticket = createValidTicketEntity(null);
        ticket.setRowNumber(row);
        ticket.setSeatNumber(seat);
        return ticket;
    }
}