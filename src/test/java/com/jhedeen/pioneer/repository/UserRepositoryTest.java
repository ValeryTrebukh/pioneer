package com.jhedeen.pioneer.repository;

import com.jhedeen.pioneer.entity.UserEntity;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.List;

import static com.jhedeen.pioneer.testutil.UserHelper.createValidUserEntity;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

public class UserRepositoryTest extends RepositoryTest {

    private static final int USER_ID = 1;
    private static final String USER_EMAIL = "ro@gmail.com";
    private static final String USER_NAME = "Ромашова Ольга";
    private static final String USER_ROLE = "ADMIN";
    private static final String USER_EMAIL_NEW = "roi@gmail.com";
    private static final String USER_NAME_NEW = "Ромашова Ирина";

    @Autowired
    UserRepository sut;

    @Test
    public void shouldFindByEmail() {
        UserEntity actual = sut.findByEmail(USER_EMAIL);

        assertThat(actual.getId(), is(USER_ID));
        assertThat(actual.getName(), is(USER_NAME));
        assertThat(actual.getEmail(), is(USER_EMAIL));
        assertThat(actual.getRole(), is(USER_ROLE));
    }

    @Test
    public void shouldFindById() {
        UserEntity actual = sut.findById(USER_ID);

        assertThat(actual.getId(), is(USER_ID));
        assertThat(actual.getName(), is(USER_NAME));
        assertThat(actual.getEmail(), is(USER_EMAIL));
        assertThat(actual.getRole(), is(USER_ROLE));
    }

    @Test
    public void shouldReturnNullForNonExistingId() {
        UserEntity userEntity = sut.findById(458);

        assertNull(userEntity);
    }

    @Test
    public void shouldReturnNullForNonExistingEmail() {
        UserEntity userEntity = sut.findByEmail("ddd");

        assertNull(userEntity);
    }

    @Test
    public void shouldSaveNewUser() {
        UserEntity expected = createValidUserEntity(null);
        expected.setEmail(USER_EMAIL_NEW);
        UserEntity saved = sut.save(expected);

        int id = saved.getId();
        expected.setId(id);
        assertThat(sut.findById(id), is(expected));
    }

    @Test
    public void shouldReturnAllUsers() {
        List<UserEntity> userEntities = sut.findAll();

        assertThat(userEntities, hasSize(9));
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void shouldThrowExceptionForExistingEmail() {
        UserEntity duplicate = createValidUserEntity(null);
        duplicate.setEmail(USER_EMAIL);
        sut.save(duplicate);
    }

    @Test
    public void shouldUpdateExistingRecord() {
        UserEntity user = createValidUserEntity(USER_ID);
        assertThat(sut.findByEmail(USER_EMAIL), is(notNullValue()));
        user.setEmail(USER_EMAIL_NEW);
        user.setName(USER_NAME_NEW);

        sut.save(user);

        assertThat(sut.findById(USER_ID).getName(), is(USER_NAME_NEW));
        assertThat(sut.findByEmail(USER_EMAIL), is(nullValue()));
    }

    @Test
    public void shouldUpdateExistingUserWithoutPasswordChangeForEmptyPassword() {

        String initPass = sut.findById(USER_ID).getPassword();
        assertThat(initPass.isEmpty(), is(false));

        sut.updateWithoutPassword(USER_NAME_NEW, USER_EMAIL_NEW, "ADMIN", USER_ID);

        UserEntity updated = sut.findById(USER_ID);

        assertThat(updated.getName(), is(USER_NAME_NEW));
        assertThat(updated.getPassword(), is(initPass));
    }

    @Test
    public void shouldRemoveCorrectUser() {
        assertThat(sut.findById(USER_ID), is(notNullValue()));

        sut.deleteById(USER_ID);

        assertThat(sut.findById(USER_ID), is(nullValue()));
    }
}