package com.jhedeen.pioneer.repository;

import com.jhedeen.pioneer.entity.EventEntity;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.jhedeen.pioneer.testutil.EventHelper.createValidEventEntity;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;

public class EventRepositoryTest extends RepositoryTest {

    private static final LocalDate FEB_13 = LocalDate.parse("2019-02-13");
    private static final LocalDate APR_15 = LocalDate.parse("2019-04-15");
    private static final int ID = 1;

    @Autowired
    EventRepository sut;

    @Test
    public void shouldFindEvent() {
        EventEntity expected = createValidEventEntity(ID);

        assertThat(expected, is(sut.findById(ID)));
    }

    @Test
    public void shouldFindByDate() {
        List<EventEntity> events = sut.findByDate(FEB_13);

        List<Integer> mids = events.stream().map(e -> e.getMovie().getId()).collect(Collectors.toList());

        assertThat(events, hasSize(3));
        assertThat(events.stream().allMatch(e -> e.getDate().equals(FEB_13)), is(true));
        assertThat(mids, is(Arrays.asList(1, 3, 7)));
    }

    @Test
    public void shouldSaveNewEvent() {
        EventEntity event = createValidEventEntity(null);
        event.setDate(APR_15);

        int id = sut.save(event).getId();

        assertThat(sut.findById(id), is(event));
    }

    @Test
    public void shouldDeleteById() {
        assertNotNull(sut.findById(ID));

        sut.deleteById(ID);

        assertNull(sut.findById(ID));
    }

    @Test
    public void shouldReturnNullForNonExistingId() {
        assertNull(sut.findById(404));
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void shouldThrowExceptionForSavingDuplicate() {
        sut.save(createValidEventEntity(null));
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void shouldNotSaveWithInvalidMovie() {
        EventEntity event = createValidEventEntity(null);
        event.getMovie().setId(19);
        event.setDate(APR_15);

        sut.save(event);
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void shouldNotSaveWithInvalidSeance() {
        EventEntity event = createValidEventEntity(null);
        event.getSeance().setId(5);
        event.setDate(APR_15);

        sut.save(event);
    }
}