DROP TABLE IF EXISTS tickets;
DROP TABLE IF EXISTS events;
DROP TABLE IF EXISTS seances;
DROP TABLE IF EXISTS movies;
DROP TABLE IF EXISTS users;


CREATE TABLE users
(
  uid         INTEGER AUTO_INCREMENT PRIMARY KEY,
  name        VARCHAR (255) NOT NULL,
  email       VARCHAR (255) UNIQUE  NOT NULL ,
  password    VARCHAR (255) NOT NULL,
  role        VARCHAR(12) NOT NULL,
  check(role in ('CLIENT', 'ADMIN'))
);

CREATE TABLE movies
(
  mid         INTEGER AUTO_INCREMENT PRIMARY KEY,
  name        VARCHAR (255) NOT NULL,
  genre       VARCHAR (255) NOT NULL,
  duration    INTEGER NOT NULL,
  year        INTEGER NOT NULL,
  active      BOOLEAN NOT NULL
);

CREATE TABLE seances
(
  sid         INTEGER AUTO_INCREMENT PRIMARY KEY,
  time        TIME NOT NULL
);

CREATE TABLE events
(
  eid         INTEGER AUTO_INCREMENT PRIMARY KEY,
  movie_id    INTEGER NOT NULL,
  date        DATE,
  seance_id   INTEGER,
  CONSTRAINT  seance_unique UNIQUE (seance_id, date),
  FOREIGN KEY (movie_id)    REFERENCES movies(mid) ON DELETE CASCADE,
  FOREIGN KEY (seance_id)   REFERENCES seances(sid) ON DELETE CASCADE
);

CREATE TABLE tickets
(
  tid         INTEGER AUTO_INCREMENT PRIMARY KEY,
  event_id    INTEGER NOT NULL,
  user_id     INTEGER,
  rov         INTEGER,
  seat        INTEGER,
  CONSTRAINT  seat_unique UNIQUE (event_id, rov, seat),
  FOREIGN KEY (event_id)  REFERENCES events(eid) ON DELETE CASCADE,
  FOREIGN KEY (user_id)   REFERENCES users(uid) ON DELETE CASCADE
);