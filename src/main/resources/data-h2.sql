INSERT INTO users (name, email, password, role) VALUES
  ('Ромашова Ольга', 'ro@gmail.com', '$2a$10$6iC2n6lmlgcxkdwG.TxA.OnRX95Zvk2hLRqcnyCykYU0ZosOgtv8u', 'ADMIN'),
  ('Тищенко Екатерина', 'te@gmail.com', '$2a$10$4w5ED08g8d.wC75Vjc6aRuuLjcRwhY34mYUQmVm35Dy/9zM1BFQQi', 'CLIENT'),
  ('Потапова Алеся', 'pa@gmail.com', '$2a$10$4w5ED08g8d.wC75Vjc6aRuuLjcRwhY34mYUQmVm35Dy/9zM1BFQQi', 'CLIENT'),
  ('Смирнова Ольга', 'so@gmail.com', '$2a$10$4w5ED08g8d.wC75Vjc6aRuuLjcRwhY34mYUQmVm35Dy/9zM1BFQQi', 'CLIENT'),
  ('Резник Иван', 'ri@gmail.com', '$2a$10$4w5ED08g8d.wC75Vjc6aRuuLjcRwhY34mYUQmVm35Dy/9zM1BFQQi', 'CLIENT'),
  ('Буланов Тимур', 'bt@gmail.com', '$2a$10$4w5ED08g8d.wC75Vjc6aRuuLjcRwhY34mYUQmVm35Dy/9zM1BFQQi', 'CLIENT'),
  ('Кириченко Олег', 'ko@gmail.com', '$2a$10$4w5ED08g8d.wC75Vjc6aRuuLjcRwhY34mYUQmVm35Dy/9zM1BFQQi', 'CLIENT'),
  ('Троицкий Никита', 'tn@gmail.com', '$2a$10$4w5ED08g8d.wC75Vjc6aRuuLjcRwhY34mYUQmVm35Dy/9zM1BFQQi', 'CLIENT'),
  ('Сапожникова Татьяна', 'st@gmail.com', '$2a$10$4w5ED08g8d.wC75Vjc6aRuuLjcRwhY34mYUQmVm35Dy/9zM1BFQQi', 'CLIENT');

INSERT INTO movies (name, genre, duration, year, active) VALUES
  ('Terminator', 'science-fiction action', 107, 1984, true),
  ('Titanic', 'epic romance', 195, 1997, true),
  ('Fifty Shades of Grey', 'erotic romantic drama', 125, 2015, true),
  ('Monsters, Inc.', 'computer-animated comedy', 92, 2001, true),
  ('Левиафан', 'драма', 142, 2014, true),
  ('Eight Below', 'drama', 120, 2006, false),
  ('Madagascar', 'computer-animated comedy', 86, 2005, true);

INSERT INTO seances (time) VALUES
  ('9:00:00'),
  ('13:00:00'),
  ('18:00:00'),
  ('22:00:00');

INSERT INTO events (movie_id, date, seance_id) VALUES
  (1, '2019-02-13', 2),
  (3, '2019-02-13', 4),
  (1, '2019-02-14', 2),
  (3, '2019-02-14', 4),
  (2, '2019-02-15', 3),
  (2, '2019-02-16', 3),
  (7, '2019-02-13', 1),
  (7, '2119-02-14', 1),
  (7, '2019-02-15', 1),
  (4, '2019-02-16', 1),
  (4, '2019-02-12', 1);

INSERT INTO tickets (event_id, user_id, rov, seat) VALUES
  (2, 2, 4, 4),
  (2, 2, 4, 5),
  (2, 2, 4, 6),
  (2, 3, 3, 4),
  (2, 3, 3, 5),
  (2, 3, 3, 6),
  (7, 6, 3, 4),
  (7, 6, 3, 5),
  (7, 6, 3, 6),
  (7, 3, 1, 2),
  (7, 5, 1, 7),
  (7, 4, 2, 3),
  (8, 2, 5, 6),
  (7, 7, 2, 5);