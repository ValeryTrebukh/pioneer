package com.jhedeen.pioneer.service.util;

import com.jhedeen.pioneer.model.UserDto;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserDataValidator {

    private static final Pattern EMAIL = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
            Pattern.CASE_INSENSITIVE);
    private static final Pattern NAME = Pattern.compile("^[А-Яа-яa-z-]{2,22} [А-Яа-яa-z-]{2,22}$",
            Pattern.CASE_INSENSITIVE);

    public static List<String> checkForErrors(UserDto user, boolean isPassRequired) {
        List<String> errors = new ArrayList<>();

        if(!isNameValid(user.getName())) {
            errors.add("errName");
        }
        if(!isEmailValid(user.getEmail())) {
            errors.add("errEmail");
        }
        if(isPassRequired) {
            if(user.getPassword() == null || user.getPassword().isEmpty() || user.getPassword().length() < 4) {
                errors.add("errPassLen");
            }
        }

        return errors;
    }

    public static boolean isEmailValid(String emailStr) {
        Matcher matcher = EMAIL.matcher(emailStr);
        return matcher.find();
    }

    private static boolean isNameValid(String emailStr) {
        Matcher matcher = NAME.matcher(emailStr);
        return matcher.find();
    }
}
