package com.jhedeen.pioneer.service.converter;

import com.jhedeen.pioneer.entity.SeanceEntity;
import com.jhedeen.pioneer.model.SeanceDto;

public class SeanceConverter {

    public static SeanceEntity toEntity(SeanceDto dto) {
        if(dto == null)
            return null;

        SeanceEntity entity = new SeanceEntity();
        entity.setId(dto.getId());
        entity.setTime(dto.getTime());

        return entity;
    }

    public static SeanceDto toDto(SeanceEntity entity) {
        SeanceDto dto = new SeanceDto();
        dto.setId(entity.getId());
        dto.setTime(entity.getTime());

        return dto;
    }
}
