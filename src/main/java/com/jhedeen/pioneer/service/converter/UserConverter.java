package com.jhedeen.pioneer.service.converter;

import com.jhedeen.pioneer.entity.UserEntity;
import com.jhedeen.pioneer.model.UserDto;

public class UserConverter {

    public static UserEntity toEntity(UserDto dto) {
        UserEntity entity = new UserEntity();

        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setEmail(dto.getEmail());
        entity.setPassword(dto.getPassword());
        entity.setRole(dto.getRole());

        return entity;
    }

    public static UserDto toDto(UserEntity entity) {
        UserDto dto = new UserDto();

        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setEmail(entity.getEmail());
        dto.setPassword(entity.getPassword());
        dto.setRole(entity.getRole());

        return dto;
    }
}
