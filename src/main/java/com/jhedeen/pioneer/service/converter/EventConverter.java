package com.jhedeen.pioneer.service.converter;

import com.jhedeen.pioneer.entity.EventEntity;
import com.jhedeen.pioneer.model.EventDto;
import com.jhedeen.pioneer.model.SimpleEventDto;

public class EventConverter {

    public static EventEntity toEntity(EventDto dto) {
        EventEntity entity = new EventEntity();
        entity.setId(dto.getId());
        entity.setDate(dto.getDate());
        entity.setSeance(SeanceConverter.toEntity(dto.getSeance()));
        entity.setMovie(MovieConverter.toEntity(dto.getMovie()));

        return entity;
    }

    public static EventDto toDto(EventEntity entity) {
        EventDto dto = new EventDto();
        dto.setId(entity.getId());
        dto.setDate(entity.getDate());
        dto.setSeance(SeanceConverter.toDto(entity.getSeance()));
        dto.setMovie(MovieConverter.toDto(entity.getMovie()));

        return dto;
    }

    public static SimpleEventDto toSimpleDto(EventEntity entity) {
        SimpleEventDto dto = new SimpleEventDto();
        dto.setEventDate(entity.getDate());
        dto.setEventTime(SeanceConverter.toDto(entity.getSeance()).getTime());
        dto.setEventName(MovieConverter.toDto(entity.getMovie()).getName());

        return dto;
    }

    public static SimpleEventDto toSimpleDto(EventDto dto) {
        SimpleEventDto simple = new SimpleEventDto();
        simple.setEventDate(dto.getDate());
        simple.setEventTime(dto.getSeance().getTime());
        simple.setEventName(dto.getMovie().getName());

        return simple;
    }
}
