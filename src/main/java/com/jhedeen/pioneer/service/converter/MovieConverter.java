package com.jhedeen.pioneer.service.converter;

import com.jhedeen.pioneer.entity.MovieEntity;
import com.jhedeen.pioneer.model.MovieDto;

public class MovieConverter {

    public static MovieEntity toEntity(MovieDto dto) {
        if(dto == null)
            return null;

        MovieEntity entity = new MovieEntity();
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setGenre(dto.getGenre());
        entity.setDuration(dto.getDuration());
        entity.setYear(dto.getYear());
        entity.setActive(dto.getActive());

        return entity;
    }

    public static MovieDto toDto(MovieEntity entity) {
        MovieDto dto = new MovieDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setGenre(entity.getGenre());
        dto.setDuration(entity.getDuration());
        dto.setYear(entity.getYear());
        dto.setActive(entity.getActive());

        return dto;
    }
}
