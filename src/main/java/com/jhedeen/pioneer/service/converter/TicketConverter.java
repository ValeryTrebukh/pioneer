package com.jhedeen.pioneer.service.converter;

import com.jhedeen.pioneer.entity.TicketEntity;
import com.jhedeen.pioneer.model.SimpleTicketDto;
import com.jhedeen.pioneer.model.TicketDto;

public class TicketConverter {

    public static TicketEntity toEntity(TicketDto dto) {
        TicketEntity ticket = new TicketEntity();
        ticket.setId(dto.getId());
        ticket.setUser(UserConverter.toEntity(dto.getUser()));
        ticket.setEvent(EventConverter.toEntity(dto.getEvent()));
        ticket.setRowNumber(dto.getRowNumber());
        ticket.setSeatNumber(dto.getSeatNumber());

        return ticket;
    }

    public static TicketDto toDto(TicketEntity entity) {
        TicketDto dto = new TicketDto();
        dto.setId(entity.getId());
        dto.setUser(UserConverter.toDto(entity.getUser()));
        dto.setEvent(EventConverter.toDto(entity.getEvent()));
        dto.setRowNumber(entity.getRowNumber());
        dto.setSeatNumber(entity.getSeatNumber());

        return dto;
    }

    public static SimpleTicketDto toSimpleDto(TicketEntity entity) {
        SimpleTicketDto dto = new SimpleTicketDto();
        dto.setId(entity.getId());
        dto.setUserName(UserConverter.toDto(entity.getUser()).getName());
        dto.setEvent(EventConverter.toSimpleDto(entity.getEvent()));
        dto.setRowNumber(entity.getRowNumber());
        dto.setSeatNumber(entity.getSeatNumber());

        return dto;
    }

    public static SimpleTicketDto toSimpleDto(TicketDto dto) {
        SimpleTicketDto simpleDto = new SimpleTicketDto();
        simpleDto.setId(dto.getId());
        simpleDto.setUserName(dto.getUser().getName());
        simpleDto.setEvent(EventConverter.toSimpleDto(dto.getEvent()));
        simpleDto.setRowNumber(dto.getRowNumber());
        simpleDto.setSeatNumber(dto.getSeatNumber());

        return simpleDto;
    }
}
