package com.jhedeen.pioneer.service;

import com.jhedeen.pioneer.model.MovieDto;

import java.util.List;

public interface MovieService {

    MovieDto save(MovieDto movie);

    void delete(int id);

    MovieDto get(int id);

    void update(MovieDto movie);

    List<MovieDto> getAllMovies();

    List<MovieDto> getActiveMovies();
}
