package com.jhedeen.pioneer.service;

import com.jhedeen.pioneer.model.EventDto;
import com.jhedeen.pioneer.model.TicketDto;
import com.jhedeen.pioneer.model.UserDto;

import java.util.List;

public interface TicketService {

    List<TicketDto> getAllTicketsByEvent(EventDto event);

    List<TicketDto> getActualTicketsByUser(UserDto user);

    List<TicketDto> getAllTicketsByUser(UserDto user);

    void saveAll(List<TicketDto> tickets);
}
