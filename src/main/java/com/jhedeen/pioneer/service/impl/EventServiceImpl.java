package com.jhedeen.pioneer.service.impl;

import com.jhedeen.pioneer.entity.EventEntity;
import com.jhedeen.pioneer.model.EventDto;
import com.jhedeen.pioneer.repository.EventRepository;
import com.jhedeen.pioneer.service.EventService;
import com.jhedeen.pioneer.service.converter.EventConverter;
import com.jhedeen.pioneer.service.exceptions.DuplicateEntityException;
import com.jhedeen.pioneer.service.exceptions.RecordNotFoundException;
import com.jhedeen.pioneer.service.exceptions.ValidationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class EventServiceImpl implements EventService {

    @Autowired
    private EventRepository eventRepository;

    @Override
    public List<EventDto> getAllEvents(LocalDate date) {
        return eventRepository.findByDate(date).stream().map(EventConverter::toDto).collect(Collectors.toList());
    }

    @Override
    public EventDto getEvent(int id) {
        return check(eventRepository.findById(id));
    }

    @Override
    public void delete(int id) {
        eventRepository.deleteById(id);
    }

    @Override
    public EventDto save(EventDto event) {
        if(!isValid(event)) {
            if(log.isDebugEnabled()) {
                log.error("invalid event: {}", event);
            }
            throw new ValidationException("Event is not valid");
        }
        try {
            EventEntity saved = eventRepository.save(EventConverter.toEntity(event));
            log.info("event saved into database with id = {}", saved);
            return EventConverter.toDto(saved);
        }  catch (DataIntegrityViolationException ex) {
            //TODO: invalid movie or seance id also causes this exception
            throw new DuplicateEntityException();
        }
    }

    private boolean isValid(EventDto event) {
        return event.getMovie() != null && event.getSeance() != null && event.getMovie().getActive();
    }

    private EventDto check(EventEntity entity) {
        if (entity == null) {
            throw new RecordNotFoundException();
        }
        return EventConverter.toDto(entity);
    }
}
