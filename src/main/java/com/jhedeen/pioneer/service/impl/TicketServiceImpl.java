package com.jhedeen.pioneer.service.impl;

import com.jhedeen.pioneer.entity.TicketEntity;
import com.jhedeen.pioneer.model.EventDto;
import com.jhedeen.pioneer.model.TicketDto;
import com.jhedeen.pioneer.model.UserDto;
import com.jhedeen.pioneer.repository.TicketRepository;
import com.jhedeen.pioneer.service.TicketService;
import com.jhedeen.pioneer.service.converter.EventConverter;
import com.jhedeen.pioneer.service.converter.TicketConverter;
import com.jhedeen.pioneer.service.converter.UserConverter;
import com.jhedeen.pioneer.service.exceptions.DuplicateEntityException;
import com.jhedeen.pioneer.service.exceptions.ValidationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class TicketServiceImpl implements TicketService {

    @Autowired
    private TicketRepository repository;

    @Value("${app.hall.rows}")
    private Integer rowsCount;

    @Value("${app.hall.seats}")
    private Integer seatsCount;

    @Override
    public List<TicketDto> getAllTicketsByEvent(EventDto event) {
        return repository.findAllByEvent(EventConverter.toEntity(event)).stream()
                .map(TicketConverter::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<TicketDto> getAllTicketsByUser(UserDto user) {
        return repository.findAllByUser(UserConverter.toEntity(user)).stream()
                .map(TicketConverter::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<TicketDto> getActualTicketsByUser(UserDto user) {
        return repository.findAllByUser(UserConverter.toEntity(user)).stream()
                .filter(this::isActual)
                .map(TicketConverter::toDto)
                .collect(Collectors.toList());
    }

    private boolean isActual(TicketEntity t) {
        return LocalDateTime.of(t.getEvent().getDate(), t.getEvent().getSeance().getTime()).isAfter(LocalDateTime.now());
    }

    @Override
    public void saveAll(List<TicketDto> tickets) {
        if(!tickets.stream().allMatch(this::isValid)) {
            throw new ValidationException("Invalid ticket data");
        }

        List<TicketEntity> entities = tickets.stream().map(TicketConverter::toEntity).collect(Collectors.toList());

        try {
            repository.saveAll(entities);
            log.info("tickets saved into database");
        }  catch (DataIntegrityViolationException ex) {
            //TODO: invalid user or event id also causes this exception
            throw new DuplicateEntityException();
        }
    }

    private boolean isValid(TicketDto ticket) {
        if(ticket.getRowNumber() < 1 || ticket.getRowNumber() > rowsCount) {
            throw new ValidationException("Invalid row number. Value out of range [1-" + rowsCount + "]");
        }

        if(ticket.getSeatNumber() < 1 || ticket.getSeatNumber() > seatsCount) {
            throw new ValidationException("Invalid seat number. Value out of range [1-" + seatsCount + "]");
        }

        return ticket.getUser() != null && ticket.getEvent() != null && ticket.getEvent().getDate() != null &&
                LocalDateTime.of(ticket.getEvent().getDate(), ticket.getEvent().getSeance().getTime()).isAfter(LocalDateTime.now());
    }
}