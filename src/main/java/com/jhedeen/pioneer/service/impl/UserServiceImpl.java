package com.jhedeen.pioneer.service.impl;

import com.jhedeen.pioneer.entity.UserEntity;
import com.jhedeen.pioneer.model.UserDto;
import com.jhedeen.pioneer.repository.UserRepository;
import com.jhedeen.pioneer.service.UserService;
import com.jhedeen.pioneer.service.converter.UserConverter;
import com.jhedeen.pioneer.service.exceptions.DuplicateEntityException;
import com.jhedeen.pioneer.service.exceptions.RecordNotFoundException;
import com.jhedeen.pioneer.service.exceptions.ValidationException;
import com.jhedeen.pioneer.service.util.UserDataValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


@Slf4j
@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    @Override
    public UserDto save(UserDto userDto) {
        List<String> errors = UserDataValidator.checkForErrors(userDto, true);
        if(!errors.isEmpty()) {
            String result = errors.stream().collect(Collectors.joining(":"));
            throw new ValidationException(result);
        }

        userDto.setPassword(encoder.encode(userDto.getPassword()));

        try {
            UserEntity saved = userRepository.save(UserConverter.toEntity(userDto));
            log.info("user saved into database with id = {}", saved);
            return UserConverter.toDto(saved);
        } catch (DataIntegrityViolationException ex) {
            throw new DuplicateEntityException();
        }
    }

    @Override
    public void update(UserDto userDto) {
        try {
            if(userDto.getPassword().isEmpty()) {
                List<String> errors = UserDataValidator.checkForErrors(userDto, false);
                if(!errors.isEmpty()) {
                    String result = errors.stream().collect(Collectors.joining(":"));
                    throw new ValidationException(result);
                }

                userRepository.updateWithoutPassword(userDto.getName(), userDto.getEmail(), userDto.getRole(), userDto.getId());
            } else {
                List<String> errors = UserDataValidator.checkForErrors(userDto, true);
                if(!errors.isEmpty()) {
                    String result = errors.stream().collect(Collectors.joining(":"));
                    throw new ValidationException(result);
                }

                userDto.setPassword(encoder.encode(userDto.getPassword()));
                userRepository.save(UserConverter.toEntity(userDto));
            }
            log.info("user updated in database with id = {}", userDto.getId());
        } catch (DataIntegrityViolationException ex) {
            throw new DuplicateEntityException();
        }
    }

    @Override
    public void delete(int id) {
        userRepository.deleteById(id);
    }

    @Override
    public UserDto getById(int id) {
        return check(userRepository.findById(id));
    }

    @Override
    public UserDto getByEmail(String email) {
        if(!UserDataValidator.isEmailValid(email)) {
            throw new ValidationException("Email is not valid");
        }

        return check(userRepository.findByEmail(email));
    }

    @Override
    public List<UserDto> getAllUsers() {
        return userRepository.findAll().stream().map(UserConverter::toDto).collect(Collectors.toList());
    }

    private UserDto check(UserEntity entity) {
        if (entity == null) {
            throw new RecordNotFoundException();
        }
        return UserConverter.toDto(entity);
    }
}
