package com.jhedeen.pioneer.service.impl;

import com.jhedeen.pioneer.entity.MovieEntity;
import com.jhedeen.pioneer.model.MovieDto;
import com.jhedeen.pioneer.repository.MovieRepository;
import com.jhedeen.pioneer.service.MovieService;
import com.jhedeen.pioneer.service.converter.MovieConverter;
import com.jhedeen.pioneer.service.exceptions.RecordNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class MovieServiceImpl implements MovieService {

    @Autowired
    private MovieRepository movieRepository;

    public MovieDto save(MovieDto movie) {
        MovieEntity saved = movieRepository.save(MovieConverter.toEntity(movie));
        log.info("movie saved into database with id = {}", saved);
        return MovieConverter.toDto(saved);
    }

    public void delete(int id) {
        movieRepository.deleteById(id);
    }

    public MovieDto get(int id) {
        return check(movieRepository.findById(id));
    }

    public void update(MovieDto movie) {
        movieRepository.save(MovieConverter.toEntity(movie));
    }

    public List<MovieDto> getAllMovies() {
        return movieRepository.findAll().stream().map(MovieConverter::toDto).collect(Collectors.toList());
    }

    public List<MovieDto> getActiveMovies() {
        return getAllMovies().stream().filter(MovieDto::getActive).collect(Collectors.toList());
    }

    private MovieDto check(MovieEntity entity) {
        if (entity == null) {
            throw new RecordNotFoundException();
        }
        return MovieConverter.toDto(entity);
    }
}
