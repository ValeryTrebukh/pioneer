package com.jhedeen.pioneer.service;

import com.jhedeen.pioneer.model.EventDto;
import com.jhedeen.pioneer.service.exceptions.DuplicateEntityException;
import com.jhedeen.pioneer.service.exceptions.RecordNotFoundException;
import com.jhedeen.pioneer.service.exceptions.ValidationException;

import java.time.LocalDate;
import java.util.List;

public interface EventService {

    List<EventDto> getAllEvents(LocalDate date);

    EventDto getEvent(int id) throws RecordNotFoundException;

    void delete(int id) throws RecordNotFoundException;

    EventDto save(EventDto event) throws DuplicateEntityException, ValidationException;
}
