package com.jhedeen.pioneer.service;

import com.jhedeen.pioneer.model.UserDto;
import com.jhedeen.pioneer.service.exceptions.DuplicateEntityException;
import com.jhedeen.pioneer.service.exceptions.RecordNotFoundException;
import com.jhedeen.pioneer.service.exceptions.ValidationException;

import java.util.List;

public interface UserService {

    UserDto save(UserDto dto) throws DuplicateEntityException, ValidationException;

    void update(UserDto dto) throws DuplicateEntityException, ValidationException;

    void delete(int id) throws RecordNotFoundException;

    UserDto getById(int id)  throws RecordNotFoundException;

    UserDto getByEmail(String email)  throws RecordNotFoundException, ValidationException;

    List<UserDto> getAllUsers();
}
