package com.jhedeen.pioneer.controller;

import com.jhedeen.pioneer.model.EventDto;
import com.jhedeen.pioneer.service.EventService;
import com.jhedeen.pioneer.service.exceptions.DuplicateEntityException;
import com.jhedeen.pioneer.service.exceptions.RecordNotFoundException;
import com.jhedeen.pioneer.service.exceptions.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;

@RestController
@RequestMapping("/events")
public class EventController {

    @Autowired
    private EventService eventService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EventDto> getById(@PathVariable Integer id) {
        try {
            return new ResponseEntity<>(eventService.getEvent(id), HttpStatus.OK);
        } catch (RecordNotFoundException rnf) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @GetMapping(value = "/date", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<EventDto>> getByDate(@RequestParam String date) {
        LocalDate localDate = toLocalDate(date);

        return new ResponseEntity<>(eventService.getAllEvents(localDate), HttpStatus.OK);
    }

    private static LocalDate toLocalDate(String date) {
        if (date == null || date.isEmpty()) {
            return LocalDate.now();
        }

        try {
            return LocalDate.parse(date);
        } catch (DateTimeParseException ex) {
            return LocalDate.now();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteById(@PathVariable Integer id) {
        eventService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity save(@RequestBody EventDto event) {
        if (event.getId() != null) {
            return new ResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY);
        }

        try {
            EventDto created = eventService.save(event);
            URI uriOfNewResource = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/events/{id}")
                    .buildAndExpand(created.getId()).toUri();
            return ResponseEntity.created(uriOfNewResource).build();
        } catch (ValidationException ve) {
            return new ResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY);
        } catch (DuplicateEntityException de) {
            HttpHeaders headers = new HttpHeaders();
            headers.add("Error", "There is already event on this timeslot.");
            return new ResponseEntity(headers, HttpStatus.NOT_ACCEPTABLE);
        }
    }
}
