package com.jhedeen.pioneer.controller;

import com.jhedeen.pioneer.model.MovieDto;
import com.jhedeen.pioneer.service.MovieService;
import com.jhedeen.pioneer.service.exceptions.RecordNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/movies")
public class MovieController {

    @Autowired
    private MovieService movieService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MovieDto> getById(@PathVariable Integer id) {
        try {
            return new ResponseEntity<>(movieService.get(id), HttpStatus.OK);
        } catch (RecordNotFoundException rnf) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<MovieDto>> getAllMovies() {
        return new ResponseEntity<>(movieService.getAllMovies(), HttpStatus.OK);
    }

    @GetMapping("/active")
    public ResponseEntity<List<MovieDto>> getActiveMovies() {
        return new ResponseEntity<>(movieService.getActiveMovies(), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteById(@PathVariable Integer id) {
        movieService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity save(@RequestBody MovieDto movie) {
        if (movie.getId() == null) {
            MovieDto created = movieService.save(movie);
            URI uriOfNewResource = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/movies/{id}")
                    .buildAndExpand(created.getId()).toUri();
            return ResponseEntity.created(uriOfNewResource).build();
        } else {
            return new ResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
        }
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity update(@RequestBody MovieDto movie) {
        if (movie.getId() == null) {
            return new ResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
        }

        if(isInvalid(movie)) {
            return new ResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY);
        }

        movieService.update(movie);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    private boolean isInvalid(MovieDto movie) {
        return movie.getName() == null ||
                movie.getGenre() == null ||
                movie.getDuration() == null ||
                movie.getYear() == null;
    }
}
