package com.jhedeen.pioneer.controller;

import com.jhedeen.pioneer.model.UserDto;
import com.jhedeen.pioneer.service.UserService;
import com.jhedeen.pioneer.service.exceptions.DuplicateEntityException;
import com.jhedeen.pioneer.service.exceptions.RecordNotFoundException;
import com.jhedeen.pioneer.service.exceptions.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<UserDto>> getAllUsers() {
        return new ResponseEntity<>(userService.getAllUsers(), HttpStatus.OK);
    }

    @GetMapping(value = "/email", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> getByEmail(@RequestParam String email) {
        try {
            return new ResponseEntity<>(userService.getByEmail(email), HttpStatus.OK);
        } catch (RecordNotFoundException rnf) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (ValidationException ve) {
            HttpHeaders headers = new HttpHeaders();
            headers.add("Error", ve.getMessage());
            return new ResponseEntity<>(headers, HttpStatus.NOT_ACCEPTABLE);
        }
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> getById(@PathVariable Integer id) {
        try {
            return new ResponseEntity<>(userService.getById(id), HttpStatus.OK);
        } catch (RecordNotFoundException rnf) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteById(@PathVariable Integer id) {
        userService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity save(@RequestBody UserDto userDto) {
        if(userDto.getId() != null) {
            return new ResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
        }

        try {
            UserDto created = userService.save(userDto);
            URI uriOfNewResource = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/users/{id}")
                    .buildAndExpand(created.getId()).toUri();
            return ResponseEntity.created(uriOfNewResource).build();
        } catch (ValidationException ve) {
            HttpHeaders headers = new HttpHeaders();
            headers.put("Error", Arrays.asList(ve.getMessage().split(":")));
            return new ResponseEntity(headers, HttpStatus.BAD_REQUEST);
        } catch (DuplicateEntityException de) {
            HttpHeaders headers = new HttpHeaders();
            headers.add("Error", "This email already registered");
            return new ResponseEntity(headers, HttpStatus.NOT_ACCEPTABLE);
        }
    }


    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity update(@RequestBody UserDto userDto) {
        if(userDto.getId() == null) {
            return new ResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
        }

        userService.update(userDto);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
