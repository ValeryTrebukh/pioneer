package com.jhedeen.pioneer.controller;

import com.jhedeen.pioneer.model.EventDto;
import com.jhedeen.pioneer.model.SimpleTicketDto;
import com.jhedeen.pioneer.model.TicketDto;
import com.jhedeen.pioneer.model.UserDto;
import com.jhedeen.pioneer.service.TicketService;
import com.jhedeen.pioneer.service.converter.TicketConverter;
import com.jhedeen.pioneer.service.exceptions.DuplicateEntityException;
import com.jhedeen.pioneer.service.exceptions.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/tickets")
public class TicketController {

    @Autowired
    private TicketService ticketService;

    @GetMapping(value = "/event/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TicketDto>> getByEvent(@PathVariable Integer id) {
        EventDto event = new EventDto();
        event.setId(id);
        return new ResponseEntity<>(ticketService.getAllTicketsByEvent(event), HttpStatus.OK);
    }

    @GetMapping(value = "/event/{id}/simple", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<SimpleTicketDto>> getSimpleByEvent(@PathVariable Integer id) {
        EventDto event = new EventDto();
        event.setId(id);
        return new ResponseEntity<>(ticketService.getAllTicketsByEvent(event).stream()
                .map(TicketConverter::toSimpleDto).collect(Collectors.toList()), HttpStatus.OK);
    }

    @GetMapping(value = "/user/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TicketDto>> getByUser(@PathVariable Integer id) {
        UserDto user = new UserDto();
        user.setId(id);
        return new ResponseEntity<>(ticketService.getAllTicketsByUser(user), HttpStatus.OK);
    }

    @GetMapping(value = "/user-actual/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TicketDto>> getActualByUser(@PathVariable Integer id) {
        //TODO: add validation: id == loggedUserId
        UserDto user = new UserDto();
        user.setId(id);
        return new ResponseEntity<>(ticketService.getActualTicketsByUser(user), HttpStatus.OK);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity save(@RequestBody List<TicketDto> tickets) {

        try {
            tickets.forEach(t -> t.setId(null));
            ticketService.saveAll(tickets);
            return new ResponseEntity(HttpStatus.OK);
        } catch (ValidationException ve) {
            //tickets with null event/user or for past date or seat/row out of range
            //TODO: add error message to response
            return new ResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY);
        } catch (DuplicateEntityException de) {
            // duplicate tickets or with non-existing event or user id
            return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
        }
    }
}
