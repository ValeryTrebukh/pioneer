package com.jhedeen.pioneer.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Getter
@Setter
@EqualsAndHashCode(of = {"event", "rowNumber", "seatNumber"})
@ToString(of = {"event", "rowNumber", "seatNumber"})
@Entity
@Table(name = "tickets", uniqueConstraints = {@UniqueConstraint(columnNames = {"rov", "seat", "event_id"}, name = "seat_unique")})
public class TicketEntity {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "tid")
    private Integer id;

    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "event_id", nullable = false)
    private EventEntity event;

    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @Column(name = "rov")
    private Integer rowNumber;

    @Column(name = "seat")
    private Integer seatNumber;
}
