package com.jhedeen.pioneer.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalTime;
import java.util.Set;

@Getter
@Setter
@EqualsAndHashCode(of = {"time"})
@ToString(of = {"time"})
@Entity
@Table(name = "seances")
public class SeanceEntity {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "sid")
    private Integer id;
    private LocalTime time;
    @OneToMany(mappedBy = "seance")
    @JsonIgnore
    private Set<EventEntity> event;
}
