package com.jhedeen.pioneer.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;


@Getter
@Setter
@EqualsAndHashCode(of = {"name", "email", "role"})
@ToString(of = {"name", "email", "role"})
@Entity
@Table(name = "users")
public class UserEntity {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="uid")
    private Integer id;

    private String name;

    @Column(unique=true)
    private String email;

    private String password;

    private String role;

    @OneToMany(mappedBy = "user")
    private Set<TicketEntity> tickets;
}
