package com.jhedeen.pioneer.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Getter
@Setter
@EqualsAndHashCode(of = {"name", "genre", "duration", "year"})
@ToString(exclude = {"id", "events"})
@Entity
@Table(name = "movies")
public class MovieEntity {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "mid")
    private Integer id;
    private String name;
    private String genre;
    private Integer duration;
    private Integer year;
    private Boolean active;
    @OneToMany(mappedBy = "movie")
    private Set<EventEntity> events;
}
