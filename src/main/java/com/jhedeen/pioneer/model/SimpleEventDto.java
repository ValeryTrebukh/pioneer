package com.jhedeen.pioneer.model;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
public class SimpleEventDto {

    private LocalDate eventDate;
    private LocalTime eventTime;
    private String eventName;
}
