package com.jhedeen.pioneer.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
public class TicketDto {

    private Integer id;
    private EventDto event;
    private UserDto user;
    private Integer rowNumber;
    private Integer seatNumber;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TicketDto ticketDto = (TicketDto) o;
        return  Objects.equals(rowNumber, ticketDto.rowNumber) &&
                Objects.equals(seatNumber, ticketDto.seatNumber) &&
                Objects.equals(event, ticketDto.event) &&
                Objects.equals(user, ticketDto.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(event, user, rowNumber, seatNumber);
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "event=" + event +
                ", user=" + user +
                ", row=" + rowNumber +
                ", seat=" + seatNumber +
                '}';
    }
}
