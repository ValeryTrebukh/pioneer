package com.jhedeen.pioneer.model;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Objects;

@Getter
@Setter
public class EventDto {
    private Integer id;
    private LocalDate date;
    private SeanceDto seance;
    private MovieDto movie;

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", date=" + date +
                ", seance=" + seance +
                ", movie=" + movie +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EventDto event = (EventDto) o;
        return Objects.equals(id, event.id) &&
                Objects.equals(date, event.date) &&
                Objects.equals(seance, event.seance) &&
                Objects.equals(movie, event.movie);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, seance, movie);
    }
}
