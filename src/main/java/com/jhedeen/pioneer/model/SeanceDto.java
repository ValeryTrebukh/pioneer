package com.jhedeen.pioneer.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalTime;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
public class SeanceDto {
    private Integer id;
    private LocalTime time;
    @JsonIgnore
    private Set<EventDto> event;

    @Override
    public String toString() {
        return "seance{" +
                "id=" + id +
                ", time=" + time +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SeanceDto seance = (SeanceDto) o;
        return Objects.equals(time, seance.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(time);
    }
}
