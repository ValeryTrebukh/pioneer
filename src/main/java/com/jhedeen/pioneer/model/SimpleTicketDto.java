package com.jhedeen.pioneer.model;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class SimpleTicketDto {

    private Integer id;
    private SimpleEventDto event;
    private String userName;
    private Integer rowNumber;
    private Integer seatNumber;

}
