package com.jhedeen.pioneer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class PioneerBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(PioneerBootApplication.class, args);
	}

}
