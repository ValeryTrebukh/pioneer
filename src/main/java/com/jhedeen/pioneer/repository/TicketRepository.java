package com.jhedeen.pioneer.repository;

import com.jhedeen.pioneer.entity.EventEntity;
import com.jhedeen.pioneer.entity.TicketEntity;
import com.jhedeen.pioneer.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface TicketRepository extends CrudRepository<TicketEntity, Integer> {

    Set<TicketEntity> findAllByUser(UserEntity user);

    Set<TicketEntity> findAllByEvent(EventEntity event);
}
