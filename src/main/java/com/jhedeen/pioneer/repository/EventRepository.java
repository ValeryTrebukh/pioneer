package com.jhedeen.pioneer.repository;

import com.jhedeen.pioneer.entity.EventEntity;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.List;

public interface EventRepository extends CrudRepository<EventEntity, Integer> {

    EventEntity findById(int id);

    List<EventEntity> findByDate(LocalDate date);

    EventEntity save(EventEntity event);

    void deleteById(int id);
}
