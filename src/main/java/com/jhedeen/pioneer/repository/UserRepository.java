package com.jhedeen.pioneer.repository;

import com.jhedeen.pioneer.entity.UserEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends CrudRepository<UserEntity, Integer> {

    UserEntity findByEmail(String email);

    List<UserEntity> findAll();

    UserEntity findById(int id);

    UserEntity save(UserEntity user);

    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE UserEntity u SET u.name = :name, u.role = :role, u.email = :email WHERE u.id = :id")
    void updateWithoutPassword(@Param("name") String name, @Param("email") String email,
                               @Param("role") String role, @Param("id") Integer id);

    void deleteById(int id);
}
