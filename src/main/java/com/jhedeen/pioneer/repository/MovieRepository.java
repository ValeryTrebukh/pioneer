package com.jhedeen.pioneer.repository;

import com.jhedeen.pioneer.entity.MovieEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MovieRepository extends CrudRepository<MovieEntity,Integer> {

    MovieEntity findById(int id);

    List<MovieEntity> findAll();

    MovieEntity save(MovieEntity movie);

    void deleteById(int id);
}
