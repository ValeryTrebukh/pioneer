package com.jhedeen.pioneer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
@Profile("dev")
public class PioneerSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/users/**", "/movies/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/events/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/events/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.GET, "/tickets/user/**").hasAnyRole("CLIENT", "ADMIN")
                .antMatchers(HttpMethod.POST, "/tickets/**").hasAnyRole("CLIENT", "ADMIN")
                .and().httpBasic()
                .and().csrf().disable();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource)
                .authoritiesByUsernameQuery("select email, CONCAT('ROLE_', role) from users where email=?")
                .usersByUsernameQuery("select email, password, 1 as enabled  from users where email=?");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
