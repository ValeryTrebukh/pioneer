FROM maven:3-jdk-8
ADD target/pioneer-single-1.0.jar pioneer-single.jar
EXPOSE 8090
ENTRYPOINT ["java", "-jar", "pioneer-single.jar"]
